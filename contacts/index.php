<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$title = (LANGUAGE_ID == 'en') ? 'Contacts' : 'Контакты';
$APPLICATION->SetTitle('Контакты КСК Алискино');
//$APPLICATION->SetTitle($title);
$APPLICATION->SetDirProperty("body_class", "contacts-page");
?>
    <div class="main-banner">
        <div class="slider">
            <div class="item contacts-section lazy" data-bg="<?=CFile::GetPath(\Aliskino\Helper::getSetting('UF_CONT_FON', 'askaron.settings'))?>">
                <div class="container">
                    <div class="contacts-area pb-100_sm">
                        <div class="page-title">
                            <h1><?=$title?></h1>
                        </div>
                        <div class="row">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/includes/contacts/contacts_".LANGUAGE_ID.".php"
                                )
                            );?>
                            <div class="col-xs-12 col-md-6 d-none d-block_lg d-block_md">
                                <a class="map-pict" href="<?=CFile::GetPath(\Aliskino\Helper::getSetting('UF_BIG_MINI_MAP', 'askaron.settings'))?>" data-fancybox>
                                    <picture>
                                        <source data-srcset="<?=CFile::GetPath(\Aliskino\Helper::getSetting('UF_MINI_MAP', 'askaron.settings'))?>" type="image/webp"/>
                                        <img class="lazy" data-src="<?=CFile::GetPath(\Aliskino\Helper::getSetting('UF_MINI_MAP', 'askaron.settings'))?>" alt=""/>
                                    </picture>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="map-pict d-none_lg d-none_md">
        <div class="container">
            <picture>
                <source data-srcset="<?=CFile::GetPath(\Aliskino\Helper::getSetting('UF_MINI_MAP', 'askaron.settings'))?>" type="image/webp"/>
                <img class="lazy d-block" data-src="<?=CFile::GetPath(\Aliskino\Helper::getSetting('UF_MINI_MAP', 'askaron.settings'))?>" alt=""/>
            </picture>
        </div>
    </div>
    <div class="map-block" id="map">
        <?=\Aliskino\Helper::getSetting('y_map')?>
    </div>
    <div class="qr-block d-none d-block_xs pt-30 pb-30">
        <picture>
            <source data-srcset="<?=CFile::GetPath(\Aliskino\Helper::getSetting('UF_CONT_QR', 'askaron.settings'))?>" type="image/webp"/>
            <img class="lazy d-block ml-auto mr-auto" data-src="<?=CFile::GetPath(\Aliskino\Helper::getSetting('UF_CONT_QR', 'askaron.settings'))?>" alt=""/>
        </picture>
    </div>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
