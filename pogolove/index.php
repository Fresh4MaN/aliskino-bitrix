<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle('О КСК «Алискино»');
?>
    <div class="main-banner">
        <div class="slider">
            <?$APPLICATION->IncludeComponent(
                "aliskino:index.page",
                "",
                Array(
                    'CODE' => 'pogolove'
                )
            );?>
        </div>
    </div>
    <div class="page-content">
        <div class="livestock-list bg-light">
            <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "horses",
                Array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array("ID","NAME", 'DETAIL_PICTURE', 'PREVIEW_PICTURE'),
                    "FILTER_NAME" => "",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => \Aliskino\Helper::getIBByCode('horses'),
                    "IBLOCK_TYPE" => "-",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "16",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Новости",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array("AGE","KLICHKA_".LANGUAGE_ID,"MAST_".LANGUAGE_ID,"PLACE_".LANGUAGE_ID,"PORODA_".LANGUAGE_ID,"STATUS".LANGUAGE_ID,
                        "TEXT1_".LANGUAGE_ID,"TEXT2_".LANGUAGE_ID,"TEXT3_".LANGUAGE_ID,"IMG3_TEXT".LANGUAGE_ID,"IMG2",'IMG3','SLIDER','IMG4'),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "SORT",
                    "SORT_BY2" => "ID",
                    "SORT_ORDER1" => "ASC",
                    "SORT_ORDER2" => "DESC",
                    "STRICT_SECTION_CHECK" => "N"
                )
            );?>
        </div>


        <div class="about-desc lazy" data-bg="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/about-bg.jpg">
            <div class="container">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/local/includes/about/attitude_".LANGUAGE_ID.'.php'
                    )
                );?>
            </div>
            <div class="picts-block d-flex flex-flow-row-wrap">
                <?$arImgs = \Aliskino\Constants::getAttitudeImgs();?>
                <?foreach($arImgs as $img):?>
                    <?$file = CFile::ResizeImageGet($img, array('width'=>320, 'height'=>270), BX_RESIZE_IMAGE_EXACT, true);?>
                    <a href="<?=CFile::GetPath($img);?>" data-fancybox="about-desc">
                        <picture>
                            <source data-srcset="<?=$file['src']?>" type="image/webp"/>
                            <img class="lazy" data-src="<?=$file['src']?>" alt=""/>
                        </picture>
                    </a>
                <?endforeach?>
            </div>
        </div>
  
    </div>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
