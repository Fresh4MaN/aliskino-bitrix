<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");?>

    <div class="main-banner">
        <div class="slider">
            <div class="item lazy page-404 inner-section lz-entered lz-loaded" data-bg="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/404.jpg" data-ll-status="loaded" style="background-image: url(<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/404.jpg);">
                <div class="container">
                    <div class="title upper extrabold" data-title="404">404<br><span>Ошибка</span></div>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/local/includes/404_".LANGUAGE_ID.".php"
                        )
                    );?>
                </div>
            </div>
        </div>
    </div>
<script>
    $('body').attr('class', 'page-not-found');
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>