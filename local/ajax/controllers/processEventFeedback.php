<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->IncludeComponent(
	"aliskino:feedback",
	"event",
	Array(
		'IBLOCK_ID' => \Aliskino\Helper::getIBByCode('event-form'),
		'EVENT_NAME' => 'EVENT_REQUEST',
	),
	$component
);