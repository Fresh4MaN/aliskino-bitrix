<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;

class GalleryFilter extends CBitrixComponent
{
    private function getMonths(){
        return [
            '01' => Loc::getMessage("JAN"),
            '02' => Loc::getMessage("FEB"),
            '03' => Loc::getMessage("MAR"),
            '04' => Loc::getMessage("APR"),
            '05' => Loc::getMessage("MAY"),
            '06' => Loc::getMessage("JUN"),
            '07' => Loc::getMessage("JUL"),
            '08' => Loc::getMessage("AUG"),
            '09' => Loc::getMessage("SEP"),
            '10' => Loc::getMessage("OCT"),
            '11' => Loc::getMessage("NOV"),
            '12' => Loc::getMessage("DEC"),
        ];
    }

    private function getYears(){
        $rez = [];
        $dvEvents = \CIBlockElement::GetList(
            Array("property_DATE"=>"asc"),
            Array("ACTIVE"=>"Y", "IBLOCK_ID"=>\Aliskino\Helper::getIBByCode('gallery')),
            false,
            false,
            ['PROPERTY_DATE']
        );
        while ($rez = $dvEvents->GetNext()){
            $year = explode('.', $rez['PROPERTY_DATE_VALUE'])[2];
            $this->arResult['YEARS'][$year] = $year;
            unset($year);
        }
    }

    private function getEvents(){
        $rez = [];
        $dvEvents = \CIBlockElement::GetList(
            Array("sort"=>"asc", "name"=>"asc"),
            Array("ACTIVE"=>"Y", "IBLOCK_ID"=>\Aliskino\Helper::getIBByCode('gallery')),
            false,
            false,
            ['PROPERTY_NAME_'.LANGUAGE_ID, 'ID', 'PROPERTY_GALLERY']
        );
        while ($rez = $dvEvents->GetNext()){
            $this->arResult['EVENTS'][$rez['ID']] = $rez['PROPERTY_NAME_'.strtoupper(LANGUAGE_ID).'_VALUE'];
            $this->arResult['EVENTS_GALLERY'][$rez['ID']] = $rez['ID'];
        }
    }

    private function makeFilter(){
        $request = Application::getInstance()->getContext()->getRequest();
        if(!empty($request['use_filter'])){
            if(!empty($request['month'])){
                $monthFilter = ['LOGIC' => 'OR'];
                foreach($this->arResult['YEARS'] as $year){
                    $monthFilter[] = [
                        ">=PROPERTY_DATE" => date("$year-{$request['month']}-01"),
                        "<=PROPERTY_DATE" => date("$year-{$request['month']}-31"),
                    ];
                }

                $arFilter[] = $monthFilter;
            }
            if(!empty($request['year'])){
                $yearFilter = [
                    ">=PROPERTY_DATE" => date("{$request['year']}-01-01"),
                    "<=PROPERTY_DATE" => date("{$request['year']}-12-31"),
                ];
                $arFilter[] = $yearFilter;
            }
            if(!empty($request['event'])){
                $arFilter["ID"] = $this->arResult['EVENTS_GALLERY'][$request['event']];
            }
        }
        if(!empty($request['tag'])){
            $arFilter['PROPERTY_TAGS'] = $request['tag'];
        }
        return $arFilter;
    }

    public function executeComponent(){
        \Bitrix\Main\Loader::includeModule('iblock');
        $this->getEvents();
        $this->getYears();
        $this->arResult['MONTHS'] = $this->getMonths();
        $this->arResult['TAGS'] = \Aliskino\Helper::getTagsCollection();
        $this->IncludeComponentTemplate();
        return $this->makeFilter();
    }

}