<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$uriString = $request->getRequestUri();
$uri = new Bitrix\Main\Web\Uri($uriString);
?>
<div class="main-banner">
    <div class="slider">
        <div class="item lazy" data-bg="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/gallery-bg.jpg">
            <div class="container">
                <div class="page-title mt-50">
                    <h1><?= Loc::getMessage("GALLERY") ?></h1>
                </div>
                <form class="gallery-filter row" method="post">
                    <input type="hidden" name="use_filter" value="Y">
                    <div class="col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="inp-lbl" for="input-1"><?= Loc::getMessage("CHOOSE_EVENT") ?></label>
                            <div class="form-select-wrap">
                                <select class="form-select" name="event" style="width: 100%">
                                    <option value=""><?= Loc::getMessage("ALL") ?></option>
                                    <?foreach($arResult['EVENTS'] as $id => $event):?>
                                        <option value="<?=$id?>" <?if($request['event'] == $id) echo 'selected';?>><?=$event?></option>
                                    <?endforeach?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="inp-lbl" for="input-2"><?= Loc::getMessage("YEAR_SEARCH") ?></label>
                            <div class="form-select-wrap">
                                <select class="form-select" name="year" style="width: 100%">
                                    <option value=""><?= Loc::getMessage("ALL") ?></option>
                                    <?foreach($arResult['YEARS'] as $id => $event):?>
                                        <option value="<?=$id?>" <?if($request['year'] == $id) echo 'selected';?>><?=$event?></option>
                                    <?endforeach?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="form-group">
                            <label class="inp-lbl" for="input-3"><?= Loc::getMessage("MONTH_SEARCH") ?></label>
                            <div class="form-select-wrap">
                                <select class="form-select" name="month" style="width: 100%">
                                    <option value=""><?= Loc::getMessage("ALL") ?></option>
                                    <?foreach($arResult['MONTHS'] as $id => $event):?>
                                        <option value="<?=$id?>" <?if($request['month'] == $id) echo 'selected';?>><?=$event?></option>
                                    <?endforeach?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="form-group btn-wrap">
                            <button class="btn"><?= Loc::getMessage("SHOW_RESULT") ?></button>
                        </div>
                    </div>
                </form>
                <div class="tags mt-40 pb-40">
                    <a href="<?=$uri->deleteParams(array("tag"))->getUri()?>"><?= Loc::getMessage("ALL") ?></a>
                    <?foreach($arResult['TAGS'] as $id => $event):?>
                        <a class="<?=(($request['tag'] == $id) ? 'active' : '')?>" href="<?=$uri->addParams(array("tag"=>$id))->getUri()?>">#<?=$event['NAME_'.LANGUAGE_ID]?></a>
                    <?endforeach?>
                </div>
            </div>
        </div>
    </div>
</div>
