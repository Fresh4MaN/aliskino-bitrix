<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application,
    Bitrix\Main\Context,
    Bitrix\Main\Request;
use Bitrix\Main\Localization\Loc;

class FormFeedback extends CBitrixComponent
{

    public static $result;

    public function executeComponent(){
        //Тут жуткая дичь - по умолчанию при запуске через ajax локаль меняется и в БД летят дроби с запятой вместо точки, от чего MYSQL падает к хуям.
        //Если же скрипт запускается не асинхронно, то все ок. 6 часов отлавливал ошибку БЛДЖАД
        setlocale(LC_NUMERIC, 'C');
        \Bitrix\Main\Loader::includeModule('iblock');
        $request = Context::getCurrent()->getRequest();

        if($request->isAjaxRequest()){
            $this->makeAjaxAction($request);

        }else {

            $this->IncludeComponentTemplate();
        }
    }

    public function makeAjaxAction($request){
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        $this->arResult['arFields'] = $request->getPostList()->toArray();

        $file = $this->prepareFileArray();
        if(!empty($file))
            $this->arResult['FILE'] = $file;

        $checkCapture = \Aliskino\Helper::VerifyRecaptcha($request);

        if($checkCapture['status'] == 'success'){
            self::$result = [
                'status' => $checkCapture['status'],
                'msg' => Loc::getMessage("SUCCESS")
            ];
            if($this->addNewElem()){
                $this->sendEventMessage($this->arResult['arFields'], $this->arResult['FILE']);
            }
        }elseif($checkCapture['status'] == 'error'){
            self::$result = [
                'status' => $checkCapture['status'],
                'msg' => implode("\n", $checkCapture['error'])
            ];
        }

        echo json_encode(self::$result);
        die();
    }

    private function addNewElem(){
        $el = new \CIBlockElement;

        $PROP = array_merge(['FILE' => $this->arResult['FILE']], $this->arResult['arFields']);
        unset($PROP['token']);
        unset($PROP['action']);

        $arLoadProductArray = Array(
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID"      => $this->arParams['IBLOCK_ID'],
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => $this->arResult['arFields']['NAME'],
            "ACTIVE"         => "Y",
        );
        $elID = $el->Add($arLoadProductArray);

        if(!is_int($elID)){
            self::$result['msg'] = Loc::getMessage("ERROR");
            return false;
        }

        return true;
    }

    private function sendEventMessage($arFields, $arFiles = array()){
        $res = \Bitrix\Main\Mail\Event::send(
            array(
                "EVENT_NAME" => $this->arParams['EVENT_NAME'],
                "LID" => "s1",
                "C_FIELDS" => $arFields,
                "FILE" => $arFiles
            )
        );
        if(!is_int($res->getId())){
            self::$result['msg'] = Loc::getMessage("ERROR_MAIL");
        }
    }

    private function prepareFileArray(){
        $req = Context::getCurrent()->getRequest();
        $arFiles = array();
        $file = $req->getFile("FILE");
        if($file['size'] > 0){
            move_uploaded_file($file["tmp_name"], $_SERVER["DOCUMENT_ROOT"]."/upload/".$file["name"]);
            $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/upload/".$file["name"]);
            $arFile["MODULE_ID"] = "main";
            $arFiles[] = $fid = CFile::SaveFile($arFile, "main");
            $arPhoto = CFile::MakeFileArray($fid);
            $this->arResult['arFields']['FILE'] = $arPhoto;
            unlink($_SERVER["DOCUMENT_ROOT"]."/upload/".$file["name"]);
        }
        return $arFiles;
    }
}