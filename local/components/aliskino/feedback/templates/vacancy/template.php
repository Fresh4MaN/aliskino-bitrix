<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
//\Bitrix\Main\Page\Asset::getInstance()->addJs('https://www.google.com/recaptcha/api.js?render='.\Aliskino\Helper::getSetting("public_key"));
?>
<script src="https://www.google.com/recaptcha/api.js?render=<?=\Aliskino\Helper::getSetting("public_key")?>"></script>
<div class="vacancy-form">
    <div class="ttl upper extrabold fs-18 mb-20 mt-70 mt-0_lg"><?= Loc::getMessage("TITLE") ?></div>
    <form id="feedback-form">
        <input type="hidden" name="token" id="token">
        <input type="hidden" name="action" id="action">
        <input type="hidden" name="VACANCY" value="<?=$arParams['VACANCY']?>">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    <label class="inp-lbl" for="input-1"><?= Loc::getMessage("NAME") ?></label>
                    <input class="js-required form-control" type="text" id="input-1" name="NAME" required="required"/>
                </div>
                <div class="form-group">
                    <label class="inp-lbl" for="input-2">E-mail</label>
                    <input class="js-required form-control" type="email" id="input-2" name="EMAIL" required="required"/>
                </div>
                <div class="form-group">
                    <label class="inp-lbl" for="input-3"><?= Loc::getMessage("PHONE") ?></label>
                    <input class="js-required form-control" type="text" id="input-3" name="PHONE" required="required"/>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    <label class="inp-lbl" for="input-4"><?= Loc::getMessage("LETTER") ?></label>
                    <textarea class="js-required form-control" id="input-4" name="LETTER" rows="5" cols="10" required="required"></textarea>
                </div>
                <div class="group-wrap">
                    <div class="form-group">
                        <div class="input-file-wrap">
                            <label class="inp-lbl" for="file"><?= Loc::getMessage("RESUME") ?></label>
                            <div class="input-file">
                                <div class="icon-wrap">
                                    <svg class="icon icon_upload">
                                        <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#upload"></use>
                                    </svg>
                                </div>
                                <input type="file" name="FILE" placeholder="" id="file">
                                <div class="empty"><?= Loc::getMessage("EMPTY FILE") ?></div>
                                <div class="choice"></div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-brown js-submit" type="submit"><?= Loc::getMessage("SEND") ?></button>
                </div>
                <br>
                <div class="group-wrap">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/local/includes/forms/policy_".LANGUAGE_ID.".php"
                        )
                    );?>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    showRecaptureIcon = true;
    grecaptcha.ready(function() {
        grecaptcha.execute('<?=\Aliskino\Helper::getSetting("public_key")?>', {action: 'vacancy'}).then(function(token) {
            if (token) {
                $('#token').val(token);
                $('#action').val('vacancy');

                // ставим обновление раз в минуту, чтобы токен не протухал
                setInterval(function () {
                    grecaptcha.execute('<?=\Aliskino\Helper::getSetting("public_key")?>', {action: 'vacancy'}).then(function(token) {
                        if (token) {
                            $('#token').val(token);
                        }
                    });
                }, 60000);
            }
        });
    });
</script>

<a href="#popup-report" data-fancybox=""></a>
<div class="popup text-center pl-30 pr-30" id="popup-report">
    <div class="title h3 extrabold upper text-center"><?= Loc::getMessage("ACCEPT") ?></div>
    <div class="text fs-18"><?= Loc::getMessage("SOON") ?></div>
    <div class="text-center mt-50">
        <button class="btn btn-black js-submit" type="button" data-fancybox-close><?= Loc::getMessage("close") ?></button>
    </div>
</div>