<?
$MESS["TITLE"] = "Leave a request for a vacancy";
$MESS["NAME"] = "Name";
$MESS["PHONE"] = "Phone";
$MESS["LETTER"] = "Write a cover letter";
$MESS["RESUME"] = "Attach resume";
$MESS["EMPTY FILE"] = " file not attached";
$MESS["SEND"] = "Send";
$MESS["ACCEPT"] = "Your application is accepted.";
$MESS["SOON"] = "Our staff will contact you shortly.";
$MESS["close"] = "Close";
