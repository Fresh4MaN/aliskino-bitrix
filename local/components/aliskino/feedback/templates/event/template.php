<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
//\Bitrix\Main\Page\Asset::getInstance()->addJs('https://www.google.com/recaptcha/api.js?render='.\Aliskino\Helper::getSetting("public_key"));
?>
<script src="https://www.google.com/recaptcha/api.js?render=<?=\Aliskino\Helper::getSetting("public_key")?>"></script>
<div class="popup" id="popup">
    <div class="title h3 extrabold upper text-center"><?= Loc::getMessage("JOIN") ?></div>
    <form class="event-form">
        <input type="hidden" name="token" class="token">
        <input type="hidden" name="action" class="action">
        <input type="hidden" name="EVENT" value="<?=$arParams['EVENT']?>">
        <div class="form-group">
            <label class="inp-lbl" for="input-1"><?= Loc::getMessage("NAME") ?></label>
            <input class="form-control" type="text" name="NAME" id="input-1"/>
        </div>
        <div class="form-group">
            <label class="inp-lbl" for="input-2">E-mail</label>
            <input class="form-control" type="email" name="EMAIL" id="input-2"/>
        </div>
        <div class="form-group">
            <label class="inp-lbl" for="input-3"><?= Loc::getMessage("PHONE") ?></label>
            <input class="form-control" type="text" name="PHONE" id="input-3"/>
        </div>
        <div class="text-center mt-50">
            <button class="btn btn-black js-submit" type="submit"><?= Loc::getMessage("SEND") ?></button>
            <div class="text-center mt-10">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/local/includes/forms/policy_".LANGUAGE_ID.".php"
                    )
                );?>
            </div>
        </div>
    </form>
</div>
<script>
    setTimeout(function(){
        grecaptcha.ready(function () {
            grecaptcha.execute('<?=\Aliskino\Helper::getSetting("public_key")?>', {action: 'event'}).then(function (token) {
                if (token) {
                    $('.token').val(token);
                    $('.action').val('event');

                    // ставим обновление раз в минуту, чтобы токен не протухал
                    setInterval(function () {
                        grecaptcha.execute('<?=\Aliskino\Helper::getSetting("public_key")?>', {action: 'event'}).then(function (token) {
                            if (token) {
                                $('.token').val(token);
                            }
                        });
                    }, 60000);
                }
            });
        });
    }, 300);
</script>

<a href="#popup-report" data-fancybox=""></a>
<div class="popup text-center pl-30 pr-30" id="popup-report">
    <div class="title h3 extrabold upper text-center"><?= Loc::getMessage("ACCEPT") ?></div>
    <div class="text fs-18"><?= Loc::getMessage("SOON") ?></div>
    <div class="text-center mt-50">
        <button class="btn btn-black js-submit" type="button" data-fancybox-close><?= Loc::getMessage("close") ?></button>
    </div>
</div>