<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class IndexPage extends CBitrixComponent
{
    private function getSelectList(){
        return array(
            'ID',
            'IBLOCK_ID',
            'DETAIL_PICTURE',
            'PREVIEW_PICTURE',
            'PROPERTY_NAME_'.LANGUAGE_ID,
            'PROPERTY_TITLE_'.LANGUAGE_ID,
            'PROPERTY_SUBTITLE_'.LANGUAGE_ID,
            'PROPERTY_BUTTON_'.LANGUAGE_ID,
            'PROPERTY_TEXT_'.LANGUAGE_ID,
            'PROPERTY_LINK',
            'PROPERTY_EVENTS',
            'PROPERTY_IS_MAIN',
            'PROPERTY_IS_EVENT',
            'PROPERTY_EXT_CLASS',
            'PROPERTY_POZ',
        );
    }

    private function getBlocks(){
        $this->arResult['EVENTS'] = [];

        $arFilter = ['ACTIVE' => 'Y', 'IBLOCK_ID' => \Aliskino\Helper::getIBByCode('index-blocks')];
        if(!empty($this->arParams['CODE']))
            $arFilter['CODE'] = $this->arParams['CODE'];

        $db = \CIBlockElement::GetList(['SORT' => 'asc'], $arFilter, false, false, $this->getSelectList());
        while($res = $db->GetNext()){
            $this->arResult['BLOCKS'][] = $res;
            if(!empty($res['PROPERTY_EVENTS_VALUE']))
                $this->arResult['EVENTS_IDS'] = array_merge($this->arResult['EVENTS'], $res['PROPERTY_EVENTS_VALUE']);
        }
    }

    private function getEvents(){
        $db = \CIBlockElement::GetList(
            ['PROPERTY_DATE' => 'asc'],
            ['ACTIVE' => 'Y', 'IBLOCK_ID' => \Aliskino\Helper::getIBByCode('events'), '>=PROPERTY_DATE' => date('Y-m-d H:i:s')],
            false,
            ['nTopCount' => 6],
            ['ID,', 'IBLOCK_ID', 'PROPERTY_DATE', 'PROPERTY_NAME_'.LANGUAGE_ID, 'DETAIL_PAGE_URL']
        );
        while($res = $db->GetNext()){
            $this->arResult['EVENTS'][$res['ID']] = $res;
        }
    }

    public function executeComponent(){
        \Bitrix\Main\Loader::includeModule('iblock');
        $this->getBlocks();
        $this->getEvents();
        $this->IncludeComponentTemplate();
    }
}
