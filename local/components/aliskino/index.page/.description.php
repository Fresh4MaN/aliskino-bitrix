<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;

$arComponentDescription = [
    "NAME" => Loc::getMessage("C_NAME"),
    "DESCRIPTION" => Loc::getMessage("C_DESCR"),
    "PATH" => [
        "ID" => "sebekon",
        "NAME" => Loc::getMessage("SE_COMPONENTS"),

    ],
];