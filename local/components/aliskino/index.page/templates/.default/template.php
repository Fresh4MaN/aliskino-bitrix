<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<?foreach($arResult['BLOCKS'] as $arBlock):?>
    <div style="background-position: <?=$arBlock['PROPERTY_POZ_VALUE']?>%" class="item <?= ($arBlock['PROPERTY_IS_MAIN_VALUE'] == 'Y') ? 'main-section' : 'inner-section'?>
        <?=$arBlock['PROPERTY_EXT_CLASS_VALUE']?> <?if(!empty($arParams['CODE'])) echo 'lazy';?>"
         data-bg="<?=(MOBILE == 'N') ? CFile::GetPath($arBlock['DETAIL_PICTURE']) : CFile::GetPath($arBlock['PREVIEW_PICTURE']);?>"
    >

        <img data-src="<?=CFile::GetPath($arBlock['DETAIL_PICTURE']);?>" alt="">
        <div class="container">
            <?if(!empty($arBlock['PROPERTY_TITLE_'.strtoupper(LANGUAGE_ID).'_VALUE'])):?>
                <div class="subtitle upper fs-16 <?= ($arBlock['PROPERTY_IS_MAIN_VALUE'] == 'Y') ? 'ml-30 ml-0_xs' : ''?>">
                    <?=$arBlock['PROPERTY_TITLE_'.strtoupper(LANGUAGE_ID).'_VALUE']?>
                </div>
            <?endif?>
            <div class="title upper extrabold <?if(strlen($arBlock['PROPERTY_NAME_'.strtoupper(LANGUAGE_ID).'_VALUE']) > 8) echo 'long-title';?>"
                 data-title="<?=$arBlock['PROPERTY_NAME_'.strtoupper(LANGUAGE_ID).'_VALUE']?>"
            >
                <?=$arBlock['PROPERTY_NAME_'.strtoupper(LANGUAGE_ID).'_VALUE']?>
             
            </div>
            <div class="bottom">
                <?if(empty($arBlock['PROPERTY_EVENTS_VALUE'])):?>
                    <div class="ttl-border d-none_xs upper fs-24"><span><?=$arBlock['PROPERTY_SUBTITLE_'.strtoupper(LANGUAGE_ID).'_VALUE']?></span></div>
                    <div class="text d-none_xs"><?=$arBlock['PROPERTY_TEXT_'.strtoupper(LANGUAGE_ID).'_VALUE']['TEXT']?></div>
                <?else:?>
                    <div class="events-list d-flex flex-flow-row-wrap d-none_xs">
                        <?$i = 0;?>
                        <?foreach($arResult['EVENTS'] as $ev):?>
                            <?if($i++ > 5) continue;?>
                            <a class="ev-item" href="<?=$ev['DETAIL_PAGE_URL']?>">
                                <div class="date"><?=$ev['PROPERTY_DATE_VALUE']?></div>
                                <div class="ttl"><?=$ev['PROPERTY_NAME_'.strtoupper(LANGUAGE_ID).'_VALUE']?></div>
                            </a>
                        <?endforeach?>
                    </div>
                <?endif?>
                <?if(!empty($arParams['CODE'])):?>
                    <a class="btn anchore-full-height" href="#<?=$arParams['CODE']?>"><?=$arBlock['PROPERTY_BUTTON_'.strtoupper(LANGUAGE_ID).'_VALUE']?></a>
                <?elseif($arBlock['PROPERTY_IS_MAIN_VALUE'] != 'Y'):?>
                    <a class="btn" href="<?=$arBlock['PROPERTY_LINK_VALUE']?>"><?=$arBlock['PROPERTY_BUTTON_'.strtoupper(LANGUAGE_ID).'_VALUE']?></a>
                <?endif?>
            </div>
        </div>
    </div>
<?endforeach?>
<?if(!empty($arParams['CODE'])):?>
<div id="<?=$arParams['CODE']?>"></div>
<?endif?>
