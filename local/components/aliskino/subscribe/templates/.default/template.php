<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
?>
<div class="subscription">
    <div class="container">
        <form class="row">
            <div class="col-md-4 col-xs-12 col-sm-6">
                <div class="ttl fs-18 upper extrabold text-center_xs mb-20_xs"><?= Loc::getMessage("SUBSCRIBE") ?></div>
            </div>
            <div class="col-md-5 col-xs-12 col-sm-6">
                <div class="form-group">
                    <label class="inp-lbl" for="input-1">E-mail</label>
                    <input class="form-control" name="email" type="email" id="input-1"/>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="form-group text-right text-center_xs text-center_sm mt-20_sm mt-20_xs">
                    <button class="btn" type="submit"><?= Loc::getMessage("SUBSCRIBE_BUTTON") ?></button>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/local/includes/forms/policy_".LANGUAGE_ID.".php"
                        )
                    );?>
                </div>
            </div>
        </form>
    </div>
</div>

<a href="#popup-report" data-fancybox=""></a>
<div class="popup text-center pl-30 pr-30" id="popup-report">
    <div class="title h3 extrabold upper text-center"><?= Loc::getMessage("ACCEPT") ?></div>
    <div class="text fs-18"><?= Loc::getMessage("SOON") ?></div>
    <div class="text-center mt-50">
        <button class="btn btn-black js-submit" type="button" data-fancybox-close><?= Loc::getMessage("close") ?></button>
    </div>
</div>