<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application,
    Bitrix\Main\Context,
    Bitrix\Main\Request;

class FormSubscribe extends CBitrixComponent
{

    public static $result;

    public function executeComponent(){
        \Bitrix\Main\Loader::includeModule('subscribe');
        $request = Context::getCurrent()->getRequest();

        if($request->isAjaxRequest()){

            $this->makeAjaxAction($request);

        }else {

            $this->IncludeComponentTemplate();
        }
    }

    public function makeAjaxAction($request){
        global $APPLICATION;
        $APPLICATION->RestartBuffer();

        if ($request['email'] && filter_var($request['email'], FILTER_VALIDATE_EMAIL)) {

            $rubricId = $this->getRubric();
            $this->createSubscribe($request['email'],[$rubricId]);

        }else{
            self::$result = [
                'status' => 'error',
                'msg' => $this->arParams['INVALID_EMAIL']
            ];
        }
        echo json_encode(self::$result);
        die();
    }

    private function getRubric(){
        $result = false;
        $rub = \CRubric::GetList(
            array("LID"=>"ASC","SORT"=>"ASC","NAME"=>"ASC"),
            array("ACTIVE"=>"Y", "CODE"=>$this->arParams['RUBRIC_CODE'])
        );
        if($arRub = $rub->Fetch()){
            $result = $arRub['ID'];
        }
        return $result;
    }

    private function createSubscribe($email, $rubricID){
        global $USER;
        $arFields = Array(
            "USER_ID" => ($USER->IsAuthorized() ? $USER->GetID() : false),
            "FORMAT" => "html",
            "EMAIL" => $email,
            "ACTIVE" => "Y",
            "RUB_ID" => $rubricID,
            "SEND_CONFIRM" => 'Y'
        );

        $subscr = new \CSubscription;

        // создаем подписку
        $ID = $subscr->Add($arFields);
        if ($ID > 0){
            self::$result = [
                'status' => 'success',
                'msg' => $this->arParams['SUCCESS']
            ];
        } else {
            self::$result = [
                'status' => 'error',
                'msg' => str_replace("<br>","",$subscr->LAST_ERROR)
            ];
        }
    }
}