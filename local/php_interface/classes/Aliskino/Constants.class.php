<?
namespace Aliskino;

class Constants {
    public static function getIblockIDDisigners() {
        return \Aliskino\Helper::getIBByCode('1');
    }

    public static function getFrontendDirectory() {
        return \Aliskino\Helper::getSetting('frontend');
    }

    public static function getAttitudeImgs(){
        return \Aliskino\Helper::getSetting('UF_ATTITUDE', 'askaron.settings');
    }

    public static function getEventImgs(){
        return \Aliskino\Helper::getSetting('UF_EVENT_IMGS', 'askaron.settings');
    }
}