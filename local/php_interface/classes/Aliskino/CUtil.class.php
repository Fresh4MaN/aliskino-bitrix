<?
namespace Aliskino;

use Bitrix\Main\Type\Date;

/**
 * Class CUtil
 */
class CUtil {

    /**
     * переводит первую букву строки в заглавную
     * @param string $strText исходный текст
     * @return string результат трансформации
     */
    public static function capitalizeString($strText)
    {
        return ToUpper(substr($strText, 0, 1)) . substr($strText, 1);
    }

    public static function YearTextArg_ru($year) {
        $year = abs($year);
        $t1 = $year % 10;
        $t2 = $year % 100;
        return ($t1 == 1 && $t2 != 11 ? "год" : ($t1 >= 2 && $t1 <= 4 && ($t2 < 10 || $t2 >= 20) ? "года" : "лет"));
    }

    public static function YearTextArg_en($year) {
        $year = abs($year);
        $t1 = $year % 10;
        $t2 = $year % 100;
        return ($t1 == 1 && $t2 != 11 ? "year" : ($t1 >= 2 && $t1 <= 4 && ($t2 < 10 || $t2 >= 20) ? "years" : "years"));
    }

    /**
     * Убирает из строки пробелы, скобочки и тире (номер телефона для подстановки в ссылку)
     * @param $str
     * @return string
     */
    public static function getPhoneForLinks($str){
        return str_replace(array(' ', '(', ')', '-'), '', $str);
    }
    /**
     * Возвращает первую запись из набора
     * @param array $arData набор записей
     * @return mixed первая запись из набора
     */
    public static function getFirst($arData) {
        return array_shift($arData);
    }

    /**
     * Обрезает текст до нужной длины
     * @param string $text текст
     * @param integer $max_len максимальная длина
     * @param bool $trim_middle обрезать посреди слова
     * @param string $trim_chars после обрезания добавить в конце символы
     * @return string результат обработки
     */
    public static function smartTrim($text, $max_len, $trim_middle = false, $trim_chars = '...')
    { // smartly trims text to desired length
        $text = trim($text);

        if (strlen(strip_tags($text)) < $max_len) {

            return strip_tags($text);

        } elseif ($trim_middle) {

            $hasSpace = strpos($text, ' ');
            if (!$hasSpace) {
                $first_half = substr($text, 0, $max_len / 2);
                $last_half = substr($text, -($max_len - strlen($first_half)));
            } else {
                $last_half = substr($text, -($max_len / 2));
                $last_half = trim($last_half);
                $last_space = strrpos($last_half, ' ');
                if (!($last_space === false)) {
                    $last_half = substr($last_half, $last_space + 1);
                }
                $first_half = substr($text, 0, $max_len - strlen($last_half));
                $first_half = trim($first_half);
                if (substr($text, $max_len - strlen($last_half), 1) == ' ') {
                    $first_space = $max_len - strlen($last_half);
                } else {
                    $first_space = strrpos($first_half, ' ');
                }
                if (!($first_space === false)) {
                    $first_half = substr($text, 0, $first_space);
                }
            }

            return $first_half.$trim_chars.$last_half;

        } else {

            $trimmed_text = strip_tags($text);
            $trimmed_text = substr($text, 0, $max_len);
            $trimmed_text = trim($trimmed_text);
            if (substr($text, $max_len, 1) == ' ') {
                $last_space = $max_len;
            } else {
                $last_space = strrpos($trimmed_text, ' ');
            }
            if (!($last_space === false)) {
                $trimmed_text = substr($trimmed_text, 0, $last_space);
            }
            return $trimmed_text.$trim_chars;
        }
    }

    /**
     * Переводит данные в JSON формат и выводит в ответе
     * @param var $varReply данные для вывода
     */
    public static function showJsonReply($varReply) {
        $strReply = json_encode($varReply);

        self::showAjaxReply($strReply);
    }

    /**
     * Формирует ответ на ajax запрос
     * @param string $strReply данные для вывода
     */
    public static function showAjaxReply($strReply) {
        if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
            require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
        }

        $GLOBALS["APPLICATION"] -> RestartBuffer();

        echo $strReply;

        require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
        die();
    }

    public static function Localredirect301($strUrl) {
        LocalRedirect($strUrl, false, '301 Moved permanently');
    }
}