<?
namespace Aliskino;


use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\PropertyEnumerationTable;
use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Data\Cache;
use Bitrix\Main\Loader;
use Bitrix\Main\ORM\Query\Query;
use Bitrix\Main\SiteTable;
use Bitrix\Main\Type\Date;

class Helper
{

    /**
     * Возвращает ID инфоблока по символьному коду. Хранение осуществляется в Registry
     * @param $strCode string символьный код инфоблока
     * @param array $arAdditionalFilter дополнительный фильтр
     * @return int ID инфоблока
     */
    public static function getIBByCode($strCode, $arAdditionalFilter = array())
    {
        if(!\Bitrix\Main\Loader::includeModule('iblock')) return;

        $arFilter = array("CODE" => $strCode);
        if (!empty($arAdditionalFilter) && is_array($arAdditionalFilter))
            $arFilter = array_merge($arAdditionalFilter, $arFilter);

        $arIblock = \Bitrix\Iblock\IblockTable::getList(
            array(
                "filter" => $arFilter
            )
        )->fetch();

        return $arIblock["ID"];
    }

    /**
     * Возвращает значения спискового свойства HL по его XML_ID
     * @string $xmlID спискового свойства
     * @string $return поле для возврата
     * @return mixed значения спискового свойства
     */
    public static function getHLEnumByXML($xmlID, $userFieldID = '', $return = 'ID')
    {
        $arFilter["XML_ID"] = $xmlID;
        if(!empty($userFieldID))
            $arFilter['USER_FIELD_ID'] = $userFieldID;

        $rsField = \CUserFieldEnum::GetList(array(), $arFilter);
        if($arField = $rsField->GetNext())
            return $arField[$return];

    }


    /**
     * Выводи информацию для дебага
     * @param $var дамп данных
     * @param bool $isAdmin выводить только для админа
     * @param null $caller @param null $caller источник вызова
     * @return bool
     */
    public static function _d($var, $isAdmin = false, $caller = null)
    {

        global $USER;
        if (!($USER->IsAdmin() || $isAdmin))
            return false;
        if (!isset($caller)) {
            $caller = array_shift(debug_backtrace(1));
        }

        echo '<code>File: ' . $caller['file'] . ' / Line: ' . $caller['line'] . '</code>';
        echo '<pre>';
        echo print_r($var, true);
        echo '</pre>';

        return true;
    }

    /**
     * Возвращает дерево разделов инфоблока
     * @param $iblockId
     * @param $customFilter
     * @return array|mixed
     */
    public static function getSectionTree($iblockId, $customFilter = array())
    {
        $arFilter = array(
            '=ACTIVE' => 'Y',
            'IBLOCK_ID' => $iblockId,
            'GLOBAL_ACTIVE' => 'Y',
            'CNT_ACTIVE' => 'Y' // поосторожнее с этим
        );
        if (!empty($customFilter)) {
            $arFilter = array_merge($arFilter, $customFilter);
        }

        if(!isset($arFilter['CHECK_PERMISSIONS'])) {
            $arFilter['CHECK_PERMISSIONS'] = 'N';
        }

        $arSelect = array('IBLOCK_ID', 'ID', 'NAME', 'DEPTH_LEVEL', 'IBLOCK_SECTION_ID', 'ACTIVE', 'SECTION_PAGE_URL');
        $arOrder = array('DEPTH_LEVEL' => 'ASC', 'SORT' => 'ASC');
        $rsSections = \CIBlockSection::GetList($arOrder, $arFilter, true, $arSelect);
        $sectionLinc = array();
        $arResult['ROOT'] = array();
        $sectionLinc[0] = &$arResult['ROOT'];
        while ($arSection = $rsSections->GetNext()) {
            $sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['CHILD'][$arSection['ID']] = $arSection;
            $sectionLinc[$arSection['ID']] = &$sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['CHILD'][$arSection['ID']];
        }
        unset($sectionLinc);
        return $arResult['ROOT']['CHILD'];
    }

    /**
     * Возвращает "листья" разделов инфоблока
     * @param $iblockID
     * @param bool $arSection
     * @return array
     */
    public static function getLastLevelSections($iblockID, $arSection = false)
    {
        if (!$arSection) {
            $tree = self::getSectionTree($iblockID);
            foreach ($tree as $section) {
                self::getLastLevelSections($iblockID, $section['CHILD']);
            }
        } else {
            foreach ($arSection as $section) {
                if (empty($section['CHILD'])) {
                    self::$arLeafs[$section['ID']] = $section;
                } else {
                    self::getLastLevelSections($iblockID, $section['CHILD']);
                }
            }
        }
        return self::$arLeafs;
    }

    /**
     * Проверка рекапчи
     * @param $request запрос который содержит token и action для рекапчи google
     * @return array два элемента: status(success при успешном, и error усли нет) и error(Содержит список ошибок если они есть)
     */
    public static function VerifyRecaptcha($request) {
        $arErr = [];
        if (!empty($request['token']) && !empty($request['action'])) {
            $captcha_token = $request['token'];
            $captcha_action = $request['action'];
        } else {
            $arErr[] = 'Некорректная работа каптчи';
        }

        $params = [
            'secret' => \Aliskino\Helper::getSetting("private_key"),
            'response' => $captcha_token,
            'remoteip' => $_SERVER['REMOTE_ADDR']
        ];

        $ch = curl_init('https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);
        if (!empty($response))
            $decoded_response = json_decode($response);

        if (!$decoded_response || !$decoded_response->success || $decoded_response->action != $captcha_action || $decoded_response->score < 0.6) {
            $arErr[] = 'Вы не прошли проверку на робота. Перезагрузите страницу';
        }

        if (empty($arErr)) {
            return [
                'status' => 'success',
                'error' => []
            ];
        } else {
            return [
                'status' => 'error',
                'error' => $arErr
            ];
        }
    }

    public static function getIBElementByCode($code){
        \CModule::IncludeModule('iblock');
        $resc = \CIBlockElement::GetList(Array(), Array('CODE' => $code, 'CHECK_PERMISSIONS' => 'N'), false);
        if($arrc = $resc->Fetch())
            return $arrc["ID"];
    }

    public static function getSetting($param, $module = "grain.customsettings"){
        return \Bitrix\Main\Config\Option::get($module, $param);
    }

    public static function getSectionByCode($iblock_id, $code){
        \CModule::IncludeModule('iblock');
        $res = \CIBlockSection::GetList(array(),array('IBLOCK_ID'=>$iblock_id,'CODE'=>$code));
        if($section = $res->Fetch())
            return $section['ID'];
        else
            return false;
    }


    public static function num2word($num, $words){
        $num = $num % 100;
        if ($num > 19) {
            $num = $num % 10;
        }
        switch ($num) {
            case 1: {
                return($words[0]);
            }
            case 2: case 3: case 4: {
            return($words[1]);
        }
            default: {
                return($words[2]);
            }
        }
    }

    public static function getSectionCodeByID($iblock_id, $id){
        \CModule::IncludeModule('iblock');
        $res = \CIBlockSection::GetList(array(),array('IBLOCK_ID'=>$iblock_id,'ID'=>$id));
        if($section = $res->Fetch())
            return $section['CODE'];
        else
            return false;
    }

    /**
     * Удаляет только указанные теги из строки
     * @param $string
     * @param $tags
     * @return string|string[]|null
     */
    public static function stripSomeTags($string, $tags){

        $tags = explode(',', $tags);

        foreach($tags as $tag) {

            $regexp = '#</?' . trim($tag) . '( .*?>|>)#siu';
            $string = preg_replace($regexp, '', $string);

        }

        return $string;

    }

    public static function ru_date($format, $date = false) {
        setlocale(LC_ALL, 'ru_RU.utf8');
        if ($date === false) {
            $date = time();
        }
        if ($format === '') {
            $format = '%e&nbsp;%bg&nbsp;%Y&nbsp;';
        }
        $months = explode("|", '|янв|фев|мар|апр|мая|июн|июл|авг|сент|окт|нояб|дек');
        $format = preg_replace("~\%bg~", $months[date('n', $date)], $format);
        return strftime($format, $date);
    }

    public static function en_date($format, $date = false) {
        setlocale(LC_ALL, 'ru_RU.utf8');
        if ($date === false) {
            $date = time();
        }
        if ($format === '') {
            $format = '%e&nbsp;%bg&nbsp;%Y&nbsp;';
        }
        $months = explode("|", '|jan|feb|mar|apr|may|jun|jul|aug|sept|oct|nov|dec');
        $format = preg_replace("~\%bg~", $months[date('n', $date)], $format);
        return strftime($format, $date);
    }

    /**
     * Возвращает ID веб формы по SID
     * @param $SID
     * @return mixed
     */

    public static function getFormByCode($SID = '')
    {
        if(!$SID) return;

        $rsData = \CForm::GetList($by = 'sort', $order = 'asc', array('SID' => $SID), $is_filtered);
        if ($arForm = $rsData->Fetch())
            return $arForm['ID'];

        return;

    }

    /**
     * Варианты склонения месяца
     * @param $month месяц на кириллице
     * @return mixed
     */

    public static function getMonthForm($month = '')
    {
        if(!$month) return;
        $monthes = [
            'январь' => 'января',
            'февраль' =>'февраля',
            'март' =>'марта',
            'апрель' =>'апреля',
            'май' =>'мая',
            'июнь' =>'июня',
            'июль' =>'июля',
            'август' =>'августа',
            'сентябрь' =>'сентября',
            'октябрь' =>'октября',
            'ноябрь' =>'ноября',
            'декабрь' =>'декабря',
        ];
        return ($monthes[strtolower($month)])?:'';
    }

    /**
     * функции mb_ucfirst нет в модуле mbstring (почему-то). Ее аналог
     * @param $str
     * @param string $enc
     * @return string
     */
    public static function mb_ucfirst($str, $enc = 'utf-8') {
        return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc).mb_substr($str, 1, mb_strlen($str, $enc), $enc);
    }

    /**
     * Возвращает справочник тегов.
     *
     * @return array
     */
    public static function getTagsCollection($arFilter = [])
    {
        $cache = Cache::createInstance();
        $cacheTime = 3600 * 24 * 7; // неделя
        $cacheDependencies = [
            __CLASS__,
            __FUNCTION__,
        ];
        $cacheId = md5(serialize($cacheDependencies));
        $cacheInitDir = '/app/collections/';
        $cacheEnabled = Option::get('main', 'component_managed_cache_on', 'N') === 'Y';
        $tagIblockId = self::getIBByCode('tags');

        $collection = [];

        try {
            if ($cacheEnabled && $cache->initCache($cacheTime, $cacheId, $cacheInitDir)) {
                $collection = $cache->getVars();
            } elseif ($cache->startDataCache() && $tagIblockId > 0) {
                Loader::includeModule('iblock');

                $arFilter = array_merge($arFilter, ['IBLOCK_ID' => $tagIblockId]);

                $result = \CIBlockElement::GetList(
                    ['NAME' => 'ASC'],
                    $arFilter,
                    false,
                    false,
                    [
                        'ID',
                        'NAME',
                        'CODE',
                    ]
                );

                while ($element = $result->GetNext()) {
                    $collection[$element['ID']] = [
                        'ID' => $element['ID'],
                        'NAME_ru' => $element['NAME'],
                        'NAME_en' => $element['CODE'],
                    ];
                }

                if (!is_array($collection) || empty($collection)) {
                    $cache->abortDataCache();
                }

                // Назначим тег для автоматического сброса кеша
                // при модификации данных в инфоблоке
                $taggedCache = Application::getInstance()->getTaggedCache();
                $taggedCache->startTagCache($cacheInitDir);
                $taggedCache->registerTag('iblock_id_' . $tagIblockId);
                $taggedCache->endTagCache();

                $cache->endDataCache($collection);
            }
        } catch (\Exception $e) {}

        return $collection;
    }

}