<?php

class Mobile
{
    public static function isMobile()
    {
        global $APPLICATION;

        if (array_key_exists('desktop', $_REQUEST) && $_REQUEST['desktop'] === 'y')
        {
            $APPLICATION->set_cookie("IS_DESKTOP", 'Y', time()+3600);

            self::redirect('desktop', $APPLICATION);
        }
        elseif (array_key_exists('mobile', $_REQUEST) && $_REQUEST['mobile'] === 'y')
        {
            $APPLICATION->set_cookie("IS_DESKTOP", 'Y', time()-3600);

            self::redirect('mobile', $APPLICATION);
        }
        else
        {
            $detect = new Mobile_Detect();
            if ($detect->isMobile() && !$APPLICATION->get_cookie("IS_DESKTOP"))
            {
                @define('MOBILE', 'Y');
            }elseif(isset($_GET["dev_mobile"]) && $_GET["dev_mobile"] == "mobile"){
				@define('MOBILE', 'Y');
			}else{
				@define('MOBILE', 'N');
			}
        }
    }

    private static function redirect($params, $APPLICATION)
    {
        LocalRedirect($APPLICATION->GetCurPageParam("", array($params)));
    }
}
