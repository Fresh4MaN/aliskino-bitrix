<?php

namespace Aliskino;

spl_autoload_register(function ($className) {
    $strNamespace = 'Aliskino\\';

    $intLen = strlen($strNamespace);
    if (strncmp($strNamespace, $className, $intLen) !== 0) {
        return;
    }

    $strRelativeClassName = substr($className, $intLen);

    $strClassFilename = __DIR__ .'/Aliskino/'. str_replace('\\', '/', $strRelativeClassName) . '.class.php';

    if (file_exists($strClassFilename)) {
        require ($strClassFilename);
    }

});