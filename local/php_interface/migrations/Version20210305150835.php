<?php

namespace Sprint\Migration;


class Version20210305150835 extends Version
{
    protected $description = "Настройки модулей";

    protected $moduleVersion = "3.23.4";

    public function up()
    {
        $helper = $this->getHelperManager();
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'addr_en',
  'VALUE' => 'МО, д.Aliskino, Skakovaya st., 4',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'addr_ru',
  'VALUE' => 'МО, д. Алискино, ул. Скаковая, д.4',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'coords',
  'VALUE' => '+ХХ° ХХ’ ХХ.ХХ", +ХХ° ХХ’ ХХ.ХХ"',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'email',
  'VALUE' => 'info@aliskino.ru',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'fb',
  'VALUE' => '#',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'frontend',
  'VALUE' => '/frontend/build/',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'GROUP_DEFAULT_RIGHT',
  'VALUE' => 'D',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'inst',
  'VALUE' => '#',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'phone',
  'VALUE' => '+7 985 222-95-24',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'private_key',
  'VALUE' => '6LcUIG0aAAAAAOXypGFERDLK-18uy9jn1lhKfc5C',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'public_key',
  'VALUE' => '6LcUIG0aAAAAAFkm--EkH-Z5UlqvCYbgvxti4fve',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'tg',
  'VALUE' => '#',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'yt',
  'VALUE' => '#',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
    }

    public function down()
    {
        //your code ...
    }
}
