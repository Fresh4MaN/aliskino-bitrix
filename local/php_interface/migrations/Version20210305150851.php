<?php

namespace Sprint\Migration;


class Version20210305150851 extends Version
{
    protected $description = "Почтовые события";

    protected $moduleVersion = "3.23.4";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();
        $helper->Event()->saveEventType('VACANCY_REQUEST', array (
  'LID' => 'ru',
  'EVENT_TYPE' => 'email',
  'NAME' => 'Заявка на вакансию',
  'DESCRIPTION' => '',
  'SORT' => '150',
));
        $helper->Event()->saveEventMessage('VACANCY_REQUEST', array (
  'LID' => 
  array (
    0 => 's1',
  ),
  'ACTIVE' => 'Y',
  'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
  'EMAIL_TO' => 'sanya-yarovoj@yandex.ru',
  'SUBJECT' => 'Новая заявка по вакансии',
  'MESSAGE' => 'Поступила новая заявка на вакансию
Имя: #NAME#
Телефон: #PHONE#
Email: #EMAIL#
Сопроводительное письмо: #LETTER#

Подробнее по ссылке
http://#SERVER_NAME#/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=8&type=forms&lang=ru&ID=#VACANCY#&find_section_section=-1&WF=Y',
  'BODY_TYPE' => 'text',
  'BCC' => '',
  'REPLY_TO' => '',
  'CC' => '',
  'IN_REPLY_TO' => '',
  'PRIORITY' => '',
  'FIELD1_NAME' => '',
  'FIELD1_VALUE' => '',
  'FIELD2_NAME' => '',
  'FIELD2_VALUE' => '',
  'SITE_TEMPLATE_ID' => '',
  'ADDITIONAL_FIELD' => 
  array (
  ),
  'LANGUAGE_ID' => '',
  'EVENT_TYPE' => '[ VACANCY_REQUEST ] Заявка на вакансию',
));
        $helper->Event()->saveEventType('EVENT_REQUEST', array (
  'LID' => 'ru',
  'EVENT_TYPE' => 'email',
  'NAME' => 'Заявка на мероприятие',
  'DESCRIPTION' => '',
  'SORT' => '150',
));
        $helper->Event()->saveEventType('EVENT_REQUEST', array (
  'LID' => 'en',
  'EVENT_TYPE' => 'email',
  'NAME' => '',
  'DESCRIPTION' => '',
  'SORT' => '150',
));
        $helper->Event()->saveEventMessage('EVENT_REQUEST', array (
  'LID' => 
  array (
    0 => 's1',
  ),
  'ACTIVE' => 'Y',
  'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
  'EMAIL_TO' => 'sanya-yarovoj@yandex.ru',
  'SUBJECT' => 'Новая заявка на мероприятие',
  'MESSAGE' => 'Поступила новая заявка на мероприятие<br>
 Имя: #NAME#<br>
 Телефон: #PHONE#<br>
 Email: #EMAIL#<br>
 <br>
 Подробнее по <a href="http://aliskino.co20140.tmweb.ru/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=10&type=forms&lang=ru&ID=#EVENT#&find_section_section=-1&WF=Y">ссылке</a><br>',
  'BODY_TYPE' => 'html',
  'BCC' => '',
  'REPLY_TO' => '',
  'CC' => '',
  'IN_REPLY_TO' => '',
  'PRIORITY' => '',
  'FIELD1_NAME' => '',
  'FIELD1_VALUE' => '',
  'FIELD2_NAME' => '',
  'FIELD2_VALUE' => '',
  'SITE_TEMPLATE_ID' => '',
  'ADDITIONAL_FIELD' => 
  array (
  ),
  'LANGUAGE_ID' => '',
  'EVENT_TYPE' => '[ EVENT_REQUEST ] Заявка на мероприятие',
));
    }

    public function down()
    {
        //your code ...
    }
}
