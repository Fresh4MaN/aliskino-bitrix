<div class="item">
    <div class="item-in">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <div class="desc">
                        <div class="title h3 upper extrabold" data-title="<?=$block['title']?>"><?=$block['title']?></div>
                        <div class="desc"><?=$block['value']?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pict">
            <picture>
                <source data-srcset="<?=$block['file']['SRC']?>" type="image/webp"/>
                <img class="lazy" data-src="<?=$block['file']['SRC']?>" alt=""/>
            </picture>
        </div>
    </div>
</div>
