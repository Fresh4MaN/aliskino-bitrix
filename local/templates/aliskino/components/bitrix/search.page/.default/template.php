<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>

<div class="main-banner">
    <div class="slider">
        <div class="item lazy" data-bg="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/private-police.jpg">
            <div class="container">
                <div class="page-title mt-40">
                    <h1><?=GetMessage("SEARCH_RESULT") ?></h1>
                </div>
                <form class="search-box" method="get" action="/search/">
                    <div class="form-group">
                        <input type="text" name="q" value="<?=$_REQUEST['q']?>" placeholder="Поиск по сайту" class="">
                    </div>
                    <button class="btn" type="submit">Найти
                        <svg class="icon icon_search">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH.\Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#search"></use>
                        </svg>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="page-content page-white-content <?=!count($arResult["SEARCH"])? 'empty-page': ''?>">
    <div class="container">
        <div class="block-white">
            <?if($arResult["ERROR_CODE"]!=0):?>
                <h3 style="text-align: center"><?=$arResult['ERROR_TEXT']?></h3>
            <?elseif(count($arResult["SEARCH"])>0):?>
                <div class="search-page-list">
                    <?foreach($arResult["SEARCH"] as $arItem):?>
                        <div class="itm">
                            <div class="box">
                                <a href="<?=$arItem["URL"]?>" class="title fs-24 fs-18_xs upper bold"><?=$arItem["TITLE_FORMATED"]?></a>
                                <!--<div class="desc"><?=$arItem["BODY_FORMATED"]?></div>-->
                            </div>
                        </div>
                    <?endforeach?>
                </div>
            <?else:?>
                <?ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));?>
            <?endif?>
        </div>
        <?if(!$arResult['NAV_RESULT']->NavPageCount > 1) {
            echo $arResult['NAV_STRING'];
        }?>
    </div>
</div>

