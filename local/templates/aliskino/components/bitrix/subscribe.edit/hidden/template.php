<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
    <?if(!empty($arResult["MESSAGE"])):?>
        alert("<?foreach($arResult["MESSAGE"] as $itemID=>$itemValue) echo $itemValue;?>");
    <?endif?>
    <?if(!empty($arResult["ERROR"])):?>
        alert("<?foreach($arResult["ERROR"] as $itemID=>$itemValue) echo $itemValue;?>");
    <?endif?>
</script>