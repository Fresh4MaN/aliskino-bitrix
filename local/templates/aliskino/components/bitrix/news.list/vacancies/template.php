<?use Bitrix\Main\Localization\Loc;?>
<?if(!empty($arResult['ITEMS'])):?>

    <div class="vacancies-container">
        <div class="top">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/local/includes/team/vacs.php"
                )
            );?>
            <div class="container">
                <div class="page-title text-center_xs">
                    <div class="title"><?= Loc::getMessage("VACS") ?></div>
                </div>
                <div class="filter-block">
                    <a class="active" href="#" data-filter="*"><?= Loc::getMessage("ALL") ?></a>
                    <?foreach($arResult['TYPES'] as $en => $ru):?>
                        <a href="#" data-filter=".<?=$en?>"><?=${LANGUAGE_ID}?></a>
                    <?endforeach;?>
                </div>
            </div>
        </div>
        <div class="bg-light pt-55 pb-55">
            <div class="container">
                <div class="vacancies-list row">
                    <?foreach ($arResult['ITEMS'] as $arItem):?>
                        <?php
                            echo "<pre>";
                            print_r($arItem['DETAIL_PAGE_URL']);
                            echo "</pre>";
                        ?>
                        <div class="item col-md-6 col-xs-12 d-flex <?=$arItem['PROPERTIES']['CATEGORY']['VALUE_XML_ID']?>">
                            <div class="box w-100">
                                <div class="title fs-30 extrabold upper"><?=$arItem['PROPERTIES']['NAME_'.LANGUAGE_ID]['VALUE']?></div>
                                <div class="ttl upper extrabold fs-18 mt-20 mb-20"><?= Loc::getMessage("OBYAZ") ?></div>
                                <div class="desc"><?=$arItem['PROPERTIES']['TEXT1_'.LANGUAGE_ID]['~VALUE']['TEXT']?></div>
                                <div class="ttl upper extrabold fs-18 mt-20 mb-20"><?= Loc::getMessage("USLOV") ?></div>
                                <?=$arItem['PROPERTIES']['TEXT2_'.LANGUAGE_ID]['~VALUE']['TEXT']?>
                                <div class="more-wrap mt-auto"><a class="more list upper lnk_reverse" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?= Loc::getMessage("MORE") ?></a></div>
                            </div>
                        </div>
                    <?endforeach;?>
                </div>
            </div>
        </div>
    </div>
<?endif?>
