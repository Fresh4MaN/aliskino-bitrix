<?use Bitrix\Main\Localization\Loc;?>
<div class="container">
    <div class="page-title">
        <div class="title"><?= Loc::getMessage("POGOLOVIE") ?></div>
    </div>
    <div class="list row">
        <?foreach($arResult['ITEMS'] as $arItem):?>
            <?$age = intval(date('Y')) - $arItem['PROPERTIES']['AGE']['VALUE'];?>
            <div class="item col-sm-6 col-md-3 col-xs-12">
                <a class="box" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                    <picture>
                        <source data-srcset="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" type="image/webp"/>
                        <img class="lazy" data-src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt=""/>
                    </picture>
                    <div class="info">
                        <?if(!empty($arItem['PROPERTIES']['STATUS_'.LANGUAGE_ID]['VALUE'])):?>
                            <div class="champ"><span><?=$arItem['PROPERTIES']['STATUS_'.LANGUAGE_ID]['VALUE']?></span></div>
                        <?endif?>
                        <div class="nickname upper mb-auto"><?= Loc::getMessage("KLICHKA") ?>: <div class="nick"><?=$arItem['PROPERTIES']['KLICHKA_'.LANGUAGE_ID]['VALUE']?></div>
                        </div>
                        <div class="char">
                            <div class="itm"><?= Loc::getMessage("PORODA") ?>: <?=$arItem['PROPERTIES']['PORODA_'.LANGUAGE_ID]['VALUE']?></div>
                            <div class="itm"><?= Loc::getMessage("AGE") ?>: <?=$age?> <?=call_user_func('\Aliskino\CUtil::YearTextArg_'.LANGUAGE_ID, $age)?></div>
                        </div>
                        <div class="more text-right upper fs-12 mt-40 light"><span class="lnk_reverse"><?= Loc::getMessage("MORE") ?></span></div>
                    </div>
                </a>
            </div>
        <?endforeach?>
    </div>
</div>
