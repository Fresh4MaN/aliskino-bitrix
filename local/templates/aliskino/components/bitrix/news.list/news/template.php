<?use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();?>
<?if(!empty($arResult['ITEMS'])):?>

    <div class="main-banner">
        <div class="slider">
            <div class="item lazy" data-bg="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/main-slide6.jpg">
                <div class="container">
                </div>
            </div>
        </div>
    </div>

    <div class="page-content news-page">
        <div class="news-list-wrap">
            <div class="container">
                <div class="page-title">
                    <h1><?= Loc::getMessage("NEWS") ?></h1>
                </div>
                <div class="list">
                    <?if($request['IS_AJAX'] == 'Y') {
                        $GLOBALS['APPLICATION']->RestartBuffer();
                    }?>
                    <?foreach($arResult['ITEMS'] as $arItem):?>
                        <div class="item">
                            <div class="box">
                                <div class="box-in">
                                    <div class="pict">
                                        <picture>
                                            <source data-srcset="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" type="image/webp"/>
                                            <img class="lazy" data-src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt=""/>
                                        </picture>
                                        <div class="info">
                                            <div class="date"><?=$arItem['PROPERTIES']['DATE']['VALUE']?></div>
                                            <div class="ttlfs-18 extrabold upper"><?=$arItem['PROPERTIES']['NAME_'.LANGUAGE_ID]['VALUE']?></div>
                                        </div>
                                    </div>
                                    <div class="desc">
                                        <div class="txt">
                                            <?=$arItem['PROPERTIES']['TEXT_'.LANGUAGE_ID]['~VALUE']['TEXT']?>
                                        </div>
                                    </div>
                                    <div class="more"><a class="upper" href="#"><?= Loc::getMessage("MORE") ?></a></div>
                                </div>
                            </div>
                        </div>
                    <?endforeach?>
                    <?if($request['IS_AJAX'] == 'Y') {
                        http_response_code ( 200 );
                        exit();
                    }?>
                </div>
                <?if($arResult['NAV_RESULT']->NavPageCount > $arResult['NAV_RESULT']->NavPageNomer):?>
                    <div class="text-center mt-40">
                        <a class="btn btn-black"
                           data-count="<?=$arResult['NAV_RESULT']->NavPageCount?>"
                           data-page="<?=$arResult['NAV_RESULT']->NavPageNomer?>"
                           id="show-more-news"
                           href="#"
                        ><?= Loc::getMessage("SHOW_MORE") ?></a>
                    </div>
                <?endif?>
            </div>
        </div>
    </div>
<?endif?>
