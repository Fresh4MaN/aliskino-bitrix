
<div class="announcement-tabs">
    <div class="container">
        <div class="tabs-panel-wrap">
            <div class="tabs-panel">
                <?foreach($arResult['ITEMS'] as $key => $arItem):?>
                    <div class="col <?if($key == 0) echo 'active'?>">
                        <a href="#tab<?=$key?>">
                            <picture>
                                <source data-srcset="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" type="image/webp"/>
                                <img class="lazy" data-src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt=""/>
                            </picture>
                            <div class="txt"><?=$arItem['PROPERTIES']['IMG_TITLE_'. LANGUAGE_ID]['VALUE']?></div>
                        </a>
                    </div>
                <?endforeach?>
            </div>
        </div>
    </div>
    <div class="tabs-wrap">
        <?foreach($arResult['ITEMS'] as $key => $arItem):?>
            <div class="tab <?if($key == 0) echo 'is-visible';?>" id="tab<?=$key?>">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="desc">
                                <?=$arItem['PROPERTIES']['TEXT_'. LANGUAGE_ID]['~VALUE']['TEXT']?>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="slider-wrap">
                                <div class="slider">
                                    <?foreach($arItem['PROPERTIES']['SLIDER']['VALUE'] as $slider):?>
                                        <div class="item">
                                            <a href="<?=CFile::GetPath($slider)?>" data-fancybox="gallery-tab1">
                                                <picture>
                                                    <source data-srcset="<?=CFile::GetPath($slider)?>" type="image/webp"/>
                                                    <img class="lazy" data-src="<?=CFile::GetPath($slider)?>" alt=""/>
                                                </picture>
                                            </a>
                                        </div>
                                    <?endforeach?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?endforeach?>
    </div>
</div>