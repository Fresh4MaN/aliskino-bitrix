<?use Bitrix\Main\Localization\Loc;?>
<div class="page-content rest-container">
    <div class="page-head">
        <div class="container">
            <div class="page-title text-center_xs">
                <h1><?= Loc::getMessage("CHOOSE") ?></h1>
            </div>
            <div class="filter-block">
                <?foreach($arResult['ITEMS'] as $key => $arItem):?>
                    <a class="<?if($key == 0) echo 'active'?> rest-filter" href="#" data-filter="<?=$arItem['ID']?>"><?=$arItem['PROPERTIES']['NAME_'.LANGUAGE_ID]['VALUE']?></a>
                <?endforeach?>
            </div>
        </div>
    </div>
    <div class="rest-list bg-light">

    </div>
</div>