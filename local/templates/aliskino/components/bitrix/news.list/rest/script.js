$(function(){
    getElement();
    $('.rest-filter').click(function(){
        $('.rest-filter').removeClass('active');
        $(this).addClass('active');
        getElement();
        return false;
    });
});

function getElement(){
    var id = $('.rest-filter.active').data('filter');
    $.ajax({
        url: '/local/ajax/controllers/getRestElement.php',
        data: {
            ELEMENT_ID: id,
            IS_AJAX: 'Y',
        },
        type: 'POST',
        success: function(msg){
            $('.rest-list').html(msg);
            uikit.lazy();
        }
    });
}