<?if(!empty($arResult)):?>
<nav>
    <ul>
        <?foreach($arResult as $arItem):?>
            <li><a href="<?=$arItem['LINK']?>"><?=$arItem["TEXT"]?></a></li>
        <?endforeach?>
    </ul>
</nav>
<?endif?>