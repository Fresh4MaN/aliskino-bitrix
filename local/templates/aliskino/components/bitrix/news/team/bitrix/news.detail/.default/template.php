<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use Bitrix\Main\Localization\Loc;
$APPLICATION->SetTitle($arResult['IPROPERTY_VALUES']['ELEMENT_META_TITLE']);
$uriString = '/galereya/';
$uri = new Bitrix\Main\Web\Uri($uriString);
?>
<div class="main-banner">
    <div class="slider">
        <div class="item lazy" data-bg="<?=$arResult['DETAIL_PICTURE']['SRC']?>">
            <div class="container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="/"><?= Loc::getMessage("MAIN") ?></a>
                        </li>
                        <li>
                            <svg class="icon icon_star">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#star"></use>
                            </svg>
                            <a href="/komanda/"><?= Loc::getMessage("TEAM") ?></a>
                        </li>
                        <li>
                            <svg class="icon icon_star">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#star"></use>
                            </svg>
                            <?=$arResult['PROPERTIES']['FIO_'.LANGUAGE_ID]['VALUE']?>
                        </li>
                    </ul>
                </div>
                <div class="page-title mb-60_xs">
                    <h1><?=$arResult['PROPERTIES']['FIO_'.LANGUAGE_ID]['VALUE']?></h1>
                </div>
                <div class="worker-area d-none_xs d-none_sm">
                    <?=$arResult['PROPERTIES']['TEXT_'.LANGUAGE_ID]['~VALUE']['TEXT']?>
                    <?=$arResult['PROPERTIES']['TEXT2_'.LANGUAGE_ID]['~VALUE']['TEXT']?>
                </div>
                <div class="hashtags">
                    <?foreach($arResult['TAGS'] as $id => $event):?>
                        <a href="<?=$uri->addParams(array("tag"=>$id))->getUri()?>">#<?=$event['NAME_'.LANGUAGE_ID]?></a>
                    <?endforeach?>
                </div>
                <div class="back  back-light light upper d-none_xs d-none_sm"><a href="/komanda/"><?= Loc::getMessage("BACK") ?></a></div>
            </div>
        </div>
    </div>
</div>
<div class="page-content bg-light d-none_lg d-none_md worker-area">
    <div class="container">
        <div class="worker-area">
            <?=$arResult['PROPERTIES']['TEXT_'.LANGUAGE_ID]['~VALUE']['TEXT']?>
            <?=$arResult['PROPERTIES']['TEXT2_'.LANGUAGE_ID]['~VALUE']['TEXT']?>
        </div>
        <br><br>
        <div class="back back-light light upper d-none_xs d-none_sm"><a href="/komanda/"><?= Loc::getMessage("BACK") ?></a></div>
    </div>
</div>
