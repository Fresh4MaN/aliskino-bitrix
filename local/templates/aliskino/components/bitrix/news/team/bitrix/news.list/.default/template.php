<?use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();?>
<?if(!empty($arResult['ITEMS'])):?>
    <div class="container">
        <div class="page-title text-center_xs">
            <h1><?= Loc::getMessage("OUR_TEAM") ?></h1>
        </div>
    </div>

    <div class="team-list-wrap">
        <div class="container">
            <div class="team-list row">
                <?if($request['IS_AJAX'] == 'Y') {
                    $GLOBALS['APPLICATION']->RestartBuffer();
                }
                foreach($arResult['ITEMS'] as $arItem):?>
                    <div class="item col-md-3 col-sm-6 col-xs-12 worker">
                        <a class="box" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                            <picture>
                                <source data-srcset="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" type="image/webp"/>
                                <img class="lazy" data-src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt=""/>
                            </picture>
                            <div class="info">
                                <div class="name upper"><?=$arItem['PROPERTIES']['FIO_'.LANGUAGE_ID]['VALUE']?></div>
                                <?=$arItem['PROPERTIES']['TEXT_'.LANGUAGE_ID]['~VALUE']['TEXT']?>
                            </div>
                        </a>
                    </div>
                <?endforeach;
                if($request['IS_AJAX'] == 'Y') {
                    http_response_code ( 200 );
                    exit();
                }?>
            </div>
            <?if($arResult['NAV_RESULT']->NavPageCount > $arResult['NAV_RESULT']->NavPageNomer):?>
                <div class="btn-wrap text-center mt-40 mt-20_xs">
                    <a class="btn btn-black"
                       data-count="<?=$arResult['NAV_RESULT']->NavPageCount?>"
                       data-page="<?=$arResult['NAV_RESULT']->NavPageNomer?>"
                       id="show-more-team"
                       href="#"
                    ><?= Loc::getMessage("SHOW_MORE") ?></a>
                </div>
            <?endif?>
        </div>
    </div>
<?endif?>
