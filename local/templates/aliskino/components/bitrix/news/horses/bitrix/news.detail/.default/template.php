<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->SetTitle($arResult['IPROPERTY_VALUES']['ELEMENT_META_TITLE']);
use Bitrix\Main\Localization\Loc;
$age = intval(date('Y')) - $arResult['PROPERTIES']['AGE']['VALUE'];
$uriString = '/galereya/';
$uri = new Bitrix\Main\Web\Uri($uriString);
?>
<div class="main-banner">
    <div class="slider">
        <div class="item lazy" data-bg="<?=$arResult['DETAIL_PICTURE']['SRC']?>">
            <div class="container">
                <div class="info-hourse">
                    <div class="itm">
                        <svg class="icon icon_star">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#star"></use>
                        </svg>
                        <div class="ttl upper"><?= Loc::getMessage("KLICHKA") ?></div>
                        <div class="val upper extrabold"><?=$arResult['PROPERTIES']['KLICHKA_'.LANGUAGE_ID]['VALUE']?></div>
                    </div>
                    <div class="itm">
                        <svg class="icon icon_star">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#star"></use>
                        </svg>
                        <div class="ttl upper"><?= Loc::getMessage("PORODA") ?></div>
                        <div class="val upper extrabold"><?=$arResult['PROPERTIES']['PORODA_'.LANGUAGE_ID]['VALUE']?></div>
                    </div>
                    <div class="itm">
                        <svg class="icon icon_star">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#star"></use>
                        </svg>
                        <div class="ttl upper"><?= Loc::getMessage("AGE") ?></div>
                        <div class="val upper extrabold"><?=$age?> <?=call_user_func('\Aliskino\CUtil::YearTextArg_'.LANGUAGE_ID, $age)?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-content hourse-container">
    <div class="page-head">
        <div class="container">
            <div class="back light"><a class="lnk_reverse upper" href="/pogolove/"><?= Loc::getMessage("BACK") ?></a></div>
            <div class="page-title text-center_xs has-rear">
                <!--<h1><?=$arResult['PROPERTIES']['KLICHKA_'.LANGUAGE_ID]['VALUE']?><span class="rear"><?=$arResult['PROPERTIES']['KLICHKA_'.LANGUAGE_ID]['VALUE']?></span></h1>-->
                <h1><?=$arResult['PROPERTIES']['KLICHKA_'.LANGUAGE_ID]['VALUE'].Loc::getMessage("LOSHAD_PORODY") . $arResult['PROPERTIES']['PORODA_'.LANGUAGE_ID]['VALUE']?><span class="rear"><?=$arResult['PROPERTIES']['KLICHKA_'.LANGUAGE_ID]['VALUE']?></span></h1>
            </div>
        </div>
        <div class="hourse-specifications lazy" data-bg="<?=CFile::GetPath($arResult['PROPERTIES']['IMG2']['VALUE'])?>">
            <div class="container">
                <div class="list upper">
                    <?if(!empty($arResult['PROPERTIES']['PORODA_'.LANGUAGE_ID]['VALUE'])):?>
                        <div class="itm">
                            <svg class="icon icon_star">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#star"></use>
                            </svg>
                            <div class="ttl"><?= Loc::getMessage("PORODA") ?></div>
                            <div class="val extrabold"><?=$arResult['PROPERTIES']['PORODA_'.LANGUAGE_ID]['VALUE']?></div>
                        </div>
                    <?endif?>
                    <?if(!empty($arResult['PROPERTIES']['HEIGHT_'.LANGUAGE_ID]['VALUE'])):?>
                        <div class="itm">
                            <svg class="icon icon_star">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#star"></use>
                            </svg>
                            <div class="ttl"><?= Loc::getMessage("ROST") ?></div>
                            <div class="val extrabold"><?=$arResult['PROPERTIES']['HEIGHT_'.LANGUAGE_ID]['VALUE']?></div>
                        </div>
                    <?endif?>
                    <?if(!empty($arResult['PROPERTIES']['MAST_'.LANGUAGE_ID]['VALUE'])):?>
                        <div class="itm">
                            <svg class="icon icon_star">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#star"></use>
                            </svg>
                            <div class="ttl"><?= Loc::getMessage("MAST") ?></div>
                            <div class="val extrabold"><?=$arResult['PROPERTIES']['MAST_'.LANGUAGE_ID]['VALUE']?></div>
                        </div>
                    <?endif?>
                    <?if(!empty($arResult['PROPERTIES']['PLACE_'.LANGUAGE_ID]['VALUE'])):?>
                        <div class="itm">
                            <svg class="icon icon_star">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#star"></use>
                            </svg>
                            <div class="ttl"><?= Loc::getMessage("PLACE") ?></div>
                            <div class="val extrabold"><?=$arResult['PROPERTIES']['PLACE_'.LANGUAGE_ID]['VALUE']?></div>
                        </div>
                    <?endif?>
                </div>
                <div class="pict d-none d-block_xs">
                    <picture>
                        <source data-srcset="<?=CFile::GetPath($arResult['PROPERTIES']['IMG2']['VALUE'])?>" type="image/webp"/>
                        <img class="lazy" data-src="<?=CFile::GetPath($arResult['PROPERTIES']['IMG2']['VALUE'])?>" alt=""/>
                    </picture>
                </div>
            </div>
        </div>
        <div class="desc-col bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-xs-12">
                        <div class="white-block">
                            <?=$arResult['PROPERTIES']['IMG3_TEXT_'.LANGUAGE_ID]['~VALUE']['TEXT']?>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="border-pict mt-80 mb-70 mt-40_xs mb-40_xs">
                            <a class="in" href="<?=CFile::GetPath($arResult['PROPERTIES']['IMG3']['VALUE'])?>" data-fancybox>
                                <picture>
                                    <source data-srcset="<?=CFile::GetPath($arResult['PROPERTIES']['IMG3']['VALUE'])?>" type="image/webp"/>
                                    <img class="lazy" data-src="<?=CFile::GetPath($arResult['PROPERTIES']['IMG3']['VALUE'])?>" alt=""/>
                                </picture>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hourse-gallery">
            <div class="container">
                <?=$arResult['PROPERTIES']['TEXT1_'.LANGUAGE_ID]['~VALUE']['TEXT']?>
                <?if(!empty($arResult['SLIDER'])):?>
                    <div class="carousel-wrap">
                        <!--Слайдер-->
                        <div class="carousel">
                            <?foreach($arResult['SLIDER'] as $img):?>
                                <div class="item">
                                    <div class="border-pict">
                                        <a class="in" href="<?=CFile::GetPath($img);?>" data-fancybox="gallery-houre">
                                            <picture>
                                                <source data-srcset="<?=CFile::GetPath($img);?>" type="image/webp"/>
                                                <img class="lazy" data-src="<?=CFile::GetPath($img);?>" alt=""/>
                                            </picture>
                                        </a>
                                    </div>
                                </div>
                            <?endforeach?>
                        </div>
                        <!--/Слайдер-->
                    </div>
                <?endif?>
            </div>
        </div>
        <div class="bg-light desc-container">
            <div class="container">
                <?if(!empty($arResult['TAGS'])):?>
                    <br>
                    <span>Смотрите все фото по </span>
                    <div class="hashtags">
                        <?foreach($arResult['TAGS'] as $id => $event):?>
                            <a class="" href="<?=$uri->addParams(array("tag"=>$id))->getUri()?>">#<?=$event['NAME_'.LANGUAGE_ID]?></a>
                        <?endforeach?>
                    </div>
                    <br><br>
                <?endif?>
                <?=$arResult['PROPERTIES']['TEXT2_'.LANGUAGE_ID]['~VALUE']['TEXT']?>
                <br><br>
            </div>
        </div>
        <div class="bg-light desc-container">
            <div class="pict">
                <picture>
                    <source data-srcset="<?=CFile::GetPath($arResult['PROPERTIES']['IMG4']['VALUE'])?>" type="image/webp"/>
                    <img class="lazy" data-src="<?=CFile::GetPath($arResult['PROPERTIES']['IMG4']['VALUE'])?>" alt=""/>
                </picture>
            </div>
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-md-6 col-xs-12">
                        <div class="desc">
                            <?=$arResult['PROPERTIES']['TEXT3_'.LANGUAGE_ID]['~VALUE']['TEXT']?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
