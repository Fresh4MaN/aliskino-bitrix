<?php
$db = CIBlockElement::GetList(
    [],
    ['IBLOCK_ID' => \Aliskino\Helper::getIBByCode('horse-slider'), 'ID' => $arResult['PROPERTIES']['SLIDER']['VALUE']],
    false,
    false,
    ['ID', 'IBLOCK_ID', 'NAME', 'PROPERTY_IMGS']
);
if($rez = $db->GetNext()){
   $arResult['SLIDER'] = $rez['PROPERTY_IMGS_VALUE'];
}

$arResult['TAGS'] = \Aliskino\Helper::getTagsCollection(['ID' => $arResult['PROPERTIES']['TAGS']['VALUE']]);