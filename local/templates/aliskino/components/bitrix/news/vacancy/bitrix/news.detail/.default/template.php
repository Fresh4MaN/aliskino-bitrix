<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use Bitrix\Main\Localization\Loc;
$APPLICATION->SetTitle($arResult['IPROPERTY_VALUES']['ELEMENT_META_TITLE']);
?>
<div class="main-banner">
    <div class="slider">
        <div class="item lazy" data-bg="<?=$arResult['DETAIL_PICTURE']['SRC']?>">
            <div class="container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="/"><?= Loc::getMessage("INDEX") ?></a>
                        </li>
                        <li>
                            <svg class="icon icon_star">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#star"></use>
                            </svg>
                            <a href="/komanda/"><?= Loc::getMessage("TEAM") ?></a>
                        </li>
                        <li>
                            <svg class="icon icon_star">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#star"></use>
                            </svg>
                            <?=$arResult['PROPERTIES']['NAME_'.LANGUAGE_ID]['VALUE']?>
                        </li>
                    </ul>
                </div>
                <div class="page-title mb-60_xs">
                    <h1><?=$arResult['PROPERTIES']['NAME_'.LANGUAGE_ID]['VALUE']?></h1>
                </div>
                <div class="vacancy-area d-none_xs d-none_sm pb-60">
                    <div class="ttl upper extrabold fs-18 mt-60 mb-20"><?= Loc::getMessage("OBYAZ") ?></div>
                    <div class="desc"><?=$arResult['PROPERTIES']['TEXT1_'.LANGUAGE_ID]['~VALUE']['TEXT']?></div>
                    <div class="ttl upper extrabold fs-18 mt-40 mb-20"><?= Loc::getMessage("USLOV") ?></div>
                    <?=$arResult['PROPERTIES']['TEXT2_'.LANGUAGE_ID]['~VALUE']['TEXT']?>
                    <div class="ttl upper extrabold fs-18 mt-30 mb-20"><?= Loc::getMessage("CONTACT_FACE") ?></div>
                    <?=$arResult['PROPERTIES']['CONTACT_'.LANGUAGE_ID]['VALUE']?>
                    <div class="phone bold"><a href="tel:<?=\Aliskino\CUtil::getPhoneForLinks($arResult['PROPERTIES']['CONTACT_PHONE']['VALUE'])?>"><?=$arResult['PROPERTIES']['CONTACT_PHONE']['VALUE']?></a></div>
                    <div class="mail bold"><a href="mailto:<?=$arResult['PROPERTIES']['CONTACT_MAIL']['VALUE']?>"><?=$arResult['PROPERTIES']['CONTACT_MAIL']['VALUE']?></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-content bg-light vacancy-page">
    <div class="container">
        <div class="vacancy-area d-none_lg d-none_md">
            <div class="ttl upper extrabold fs-18 mb-20"><?= Loc::getMessage("OBYAZ") ?></div>
            <div class="desc"><?=$arResult['PROPERTIES']['TEXT1_'.LANGUAGE_ID]['~VALUE']['TEXT']?></div>
            <div class="ttl upper extrabold fs-18 mt-40 mb-20"><?= Loc::getMessage("USLOV") ?></div>
            <?=$arResult['PROPERTIES']['TEXT2_'.LANGUAGE_ID]['~VALUE']['TEXT']?>
            <div class="ttl upper extrabold fs-18 mt-30 mb-20"><?= Loc::getMessage("CONTACT_FACE") ?></div>
            <?=$arResult['PROPERTIES']['CONTACT_'.LANGUAGE_ID]['VALUE']?>
            <div class="phone bold"><a href="tel:<?=\Aliskino\CUtil::getPhoneForLinks($arResult['PROPERTIES']['CONTACT_PHONE']['VALUE'])?>"><?=$arResult['PROPERTIES']['CONTACT_PHONE']['VALUE']?></a></div>
            <div class="mail bold"><a href="mailto:<?=$arResult['PROPERTIES']['CONTACT_MAIL']['VALUE']?>"><?=$arResult['PROPERTIES']['CONTACT_MAIL']['VALUE']?></a></div>
        </div>

        <?$APPLICATION->IncludeComponent(
            "aliskino:feedback",
            "vacancy",
            Array(
                'IBLOCK_ID' => \Aliskino\Helper::getIBByCode('vacancy-form'),
                'EVENT_NAME' => 'VACANCY_REQUEST',
                'VACANCY' => $arResult['ID']
            ),
            $component
        );?>


        <br><br>
        <div class="back light upper"><a href="/komanda/"><?= Loc::getMessage("BACK") ?></a></div>
    </div>
</div>
