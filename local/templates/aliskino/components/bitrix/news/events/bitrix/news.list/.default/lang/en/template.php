<?
$MESS["EVENTS"] = "Events";
$MESS["FUTURE"] = "Future events";
$MESS["PAST"] = "Past events";
$MESS["MORE"] = "more >>";
$MESS["NOT_FOUND"] = "No events matching the specified conditions were found";