<?php
$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$uriString = $request->getRequestUri();
$uri = new Bitrix\Main\Web\Uri($uriString);

$arResult['FORMATS']['All'] = ['ID' => '', 'VALUE' => 'Все'];
$property_enums = CIBlockPropertyEnum::GetList(Array("ID"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "CODE"=>"FORMAT"));
while($enum_fields = $property_enums->GetNext()){
    $arResult['FORMATS'][$enum_fields['XML_ID']] = ['ID' => $enum_fields['ID'], 'VALUE' => $enum_fields['VALUE']];
}

foreach($arResult['FORMATS'] as $key => $format){
    $arResult['FILTER_HTML'][] = '<a class="'.(($request['format'] == $format['ID']) ? 'active' : '').'" href="'.$uri->addParams(array("format"=>$format['ID']))->getUri().'" >'.((LANGUAGE_ID == 'en') ? $key : $format['VALUE']).'</a>';
}