<?use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();
$uriString = $request->getRequestUri();
$uri = new Bitrix\Main\Web\Uri($uriString);
?>
<?//if(!empty($arResult['ITEMS'])):?>
    <div class="main-banner">
        <div class="slider">
            <div class="item lazy" data-bg="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/main-slide5.jpg">
                <div class="container">
                    <div class="page-title mt-20">
                        <h1><?= Loc::getMessage("EVENTS") ?></h1>
                    </div>
                    <div class="filter-block filter-evetns ml-0 mt-auto_xs">
                        <a class="<?=(empty($request['date']) || $request['date'] == 'future') ? 'active': ''?>" href="<?=$uri->addParams(array("date"=>'future'))->getUri()?>" data-filter="*"><?= Loc::getMessage("FUTURE") ?></a>
                        <a class="<?=($request['date'] == 'past') ? 'active': ''?>" href="<?=$uri->addParams(array("date"=>'past'))->getUri()?>" data-filter=""><?= Loc::getMessage("PAST") ?></a>
                    </div>
                    <div class="events-links mt-30 mb-60_xs">
                        <?=implode('<span></span>', $arResult['FILTER_HTML'] );?>
                    </div>
                    <?if(!empty($arResult['ITEMS'])):?>
                        <div class="events-carousel mt-80">
                            <?foreach($arResult['ITEMS'] as $arItem):?>
                                <div class="itm">
                                    <a class="box" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                        <picture>
                                            <source data-srcset="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" type="image/webp"/>
                                            <img class="lazy" data-src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt=""/>
                                        </picture>
                                        <div class="info">
                                            <div class="top d-flex align-items-end">
                                                <div class="type upper light mr-10"><?=((LANGUAGE_ID == 'en') ? $arItem['PROPERTIES']['FORMAT']['VALUE_XML_ID'] : $arItem['PROPERTIES']['FORMAT']['VALUE'])?></div>
                                                <div class="date fs-10"><?=explode(' ', $arItem['PROPERTIES']['DATE']['VALUE'])[0]?></div>
                                            </div>
                                            <div class="ttl upper extrabold fs-24"><?=$arItem['PROPERTIES']['NAME_'.LANGUAGE_ID]['~VALUE']?></div>
                                            <div class="more mt-15 upper fs-12"><span><?= Loc::getMessage("MORE") ?></span></div>
                                        </div>
                                    </a>
                                </div>
                            <?endforeach?>
                        </div>
                    <?else:?>
                        <h3 class="title mt-40 mt-20_xs"><?= Loc::getMessage("NOT_FOUND") ?></h3>
                    <?endif?>
                </div>
            </div>
        </div>
    </div>
    <div class="events-carousel-wrap">
        <div class="events-carousel">
            <?if(!empty($arResult['ITEMS'])):?>
                <?foreach($arResult['ITEMS'] as $arItem):?>
                    <div class="itm">
                        <a class="box" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                            <picture>
                                <source data-srcset="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" type="image/webp"/>
                                <img class="lazy" data-src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt=""/>
                            </picture>
                            <div class="info">
                                <div class="top d-flex align-items-end">
                                    <div class="type upper light mr-10"> <?=((LANGUAGE_ID == 'en') ? $arItem['PROPERTIES']['FORMAT']['VALUE_XML_ID'] : $arItem['PROPERTIES']['FORMAT']['VALUE'])?></div>
                                    <div class="date fs-10"><?=$arItem['PROPERTIES']['DATE']['VALUE']?></div>
                                </div>
                                <div class="ttl upper extrabold fs-24"><?=$arItem['PROPERTIES']['NAME_'.LANGUAGE_ID]['~VALUE']?></div>
                                <div class="more mt-15 upper fs-12"><span><?= Loc::getMessage("MORE") ?></span></div>
                            </div>
                        </a>
                    </div>
                <?endforeach?>
            <?else:?>
                <h2 class="events-not-found"><?= Loc::getMessage("NOT_FOUND") ?></h2>
            <?endif?>
        </div>
    </div>
<?//endif?>
