<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use Bitrix\Main\Localization\Loc;
$APPLICATION->SetTitle($arResult['IPROPERTY_VALUES']['ELEMENT_META_TITLE']);
$time = explode(':', explode(' ', $arResult['PROPERTIES']['DATE']['VALUE'])[1]);
$time = $time[0].':'.$time[1];
$class = '\\Aliskino\\Helper';
$method = 'myFunction';
?>
<div class="main-banner">
    <div class="slider">
        <div class="item lazy" data-bg="<?=$arResult['DETAIL_PICTURE']['SRC']?>">
            <div class="container">
                <div class="event-area">
                    <div class="upper fs-16"><?= ((LANGUAGE_ID == 'en') ? $arResult['PROPERTIES']['FORMAT']['VALUE_XML_ID'] : $arResult['PROPERTIES']['FORMAT']['VALUE'])?></div>
                    <div class="page-title">
                        <h1>
                            <?=$arResult['PROPERTIES']['NAME_'.LANGUAGE_ID]['~VALUE']?>
                        </h1>
                    </div>
                </div>
                <div class="info-ev mt-auto pt-15 pb-25 pb-100_xs">
                    <div class="itm">
                        <div class="ttl upper fs-24">
                            <svg class="icon icon_star">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#star"></use>
                            </svg>
                            <?= Loc::getMessage("DATE") ?></div>
                        <div class="extrabold fs-20">
                            <?=call_user_func('\Aliskino\Helper::'.LANGUAGE_ID.'_date', '', strtotime($arResult['PROPERTIES']['DATE']['VALUE']))?>
                            <?//=\Aliskino\Helper::ru_date('', strtotime($arResult['PROPERTIES']['DATE']['VALUE']))?>
                            <br><?=$time?></div>
                    </div>
                    <div class="itm mt-30">
                        <div class="ttl upper fs-24">
                            <svg class="icon icon_star">
                                <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#star"></use>
                            </svg>
                            <?= Loc::getMessage("PLACE") ?></div>
                        <div class="extrabold fs-20"><?=$arResult['PROPERTIES']['PLACE_'.LANGUAGE_ID]['~VALUE']?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-content event-container">
    <div class="page-top">
        <div class="container">
            <div class="desc pb-70 pb-25_xs">
                <div class="page-title no-border upper">
                    <h2><?=$arResult['PROPERTIES']['SUBTITLE_'.LANGUAGE_ID]['~VALUE']?></h2>
                </div>
                <p><?=$arResult['PROPERTIES']['TEXT1_'.LANGUAGE_ID]['~VALUE']['TEXT']?></p>
            </div>
            <div class="pict">
                <picture>
                    <source data-srcset="<?=CFile::GetPath($arResult['PROPERTIES']['IMAGE']['VALUE'])?>" type="image/webp"/>
                    <img class="lazy" data-src="<?=CFile::GetPath($arResult['PROPERTIES']['IMAGE']['VALUE'])?>" alt=""/>
                </picture>
            </div>
        </div>
    </div>
    <div class="page-bottom bg-light pt-70 pb-70 pt-25_xs pb-25_xs">
        <div class="container">
            <div class="desc">
                <?=$arResult['PROPERTIES']['TEXT2_'.LANGUAGE_ID]['~VALUE']['TEXT']?>
            </div>
            <p class="mt-100_lg mt-20"><?=$arResult['PROPERTIES']['TEXT3_'.LANGUAGE_ID]['~VALUE']['TEXT']?></p>
        </div>
    </div>
</div>
<?if($arResult['PROPERTIES']['SHOW_SUBSC']['VALUE'] == 'Y'):?>
    <div class="signup-event pt-70 pb-70 pt-25_xs pt-25_sm pb-25_xs pb-25_sm">
        <div class="container">
            <div class="d-flex flex-flow-row-wrap justify-content-center align-items-center text-center_sm text-center_xs">
                <div class="ttl h3 mb-0 upper extrabold mb-20_xs mb-20_sm"><?= Loc::getMessage("GO_TO_EVENT") ?></div>
                <a class="btn" href="#popup" data-fancybox><?= Loc::getMessage("JOIN") ?></a>
            </div>
        </div>
    </div>
<?endif?>
<?if(!empty($arResult['PROPERTIES']['GALLERY']['VALUE'])):?>
    <div class="loock-photo bg-light pt-70 pb-70 text-center">
        <div class="container">
            <div class="ttl uppper extrabold h3"><?= Loc::getMessage("WATCH") ?></div>
            <div class="btn-wrap"><a class="btn btn-brown" href="<?=$arResult['DISPLAY_PROPERTIES']['GALLERY']['LINK_ELEMENT_VALUE'][$arResult['DISPLAY_PROPERTIES']['GALLERY']['VALUE']]['DETAIL_PAGE_URL']?>"><?= Loc::getMessage("GOTO") ?></a></div>
        </div>
    </div>
<?endif?>
<?if($arResult['PROPERTIES']['SHOW_SUBSC']['VALUE'] == 'Y'):?>
    <?$APPLICATION->IncludeComponent(
        "aliskino:feedback",
        "event",
        Array(
            'IBLOCK_ID' => \Aliskino\Helper::getIBByCode('event-form'),
            'EVENT_NAME' => 'EVENT_REQUEST',
            'EVENT' => $arResult['ID']
        ),
        $component
    );?>
<?endif?>

