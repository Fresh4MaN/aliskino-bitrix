<?use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();?>
<?if(!empty($arResult['ITEMS'])):?>
    <div class="page-content gallery-container">
        <div class="container">
            <div class="gallery-list row">
                <?foreach($arResult['ITEMS'] as $arItem):?>
                    <div class="item col-md-4 col-xs-12 col-sm-6">
                        <a class="box" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                            <div class="in">
                                <picture>
                                    <source data-srcset="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" type="image/webp"/>
                                    <img class="lazy" data-src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt=""/>
                                </picture>
                                <div class="info">
                                    <div class="date fs-10"><?=$arItem['PROPERTIES']['DATE']['VALUE']?></div>
                                    <div class="ttl upper fs-18 extrabold"><?=$arItem['PROPERTIES']['NAME_'.LANGUAGE_ID]['VALUE']?></div>
                                </div>
                            </div>
                        </a>
                    </div>
                <?endforeach?>
            </div>
            <?=$arResult['NAV_STRING']?>
        </div>
    </div>
<?endif?>
