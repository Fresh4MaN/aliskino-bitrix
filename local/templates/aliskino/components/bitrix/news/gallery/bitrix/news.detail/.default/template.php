<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use Bitrix\Main\Localization\Loc;
$APPLICATION->SetTitle($arResult['IPROPERTY_VALUES']['ELEMENT_META_TITLE']);
$tags = \Aliskino\Helper::getTagsCollection();
?>
<div class="main-banner">
    <div class="slider">
        <div class="item lazy" data-bg="<?=$arResult['DETAIL_PICTURE']['SRC']?>">
            <div class="container">
                <div class="back upper"><a href="/galereya/"><?= Loc::getMessage("BACK") ?></a></div>
                <div class="date"><?=$arResult['PROPERTIES']['DATE']['VALUE']?></div>
                <div class="page-title">
                    <h1><?=$arResult['PROPERTIES']['NAME_'.LANGUAGE_ID]['VALUE']?></h1>
                </div>
                <div class="tags mt-40 pb-40">
                    <?foreach($arResult['PROPERTIES']['TAGS']['VALUE'] as $tag):?>
                        <span>#<?=$tags[$tag]['NAME_'.LANGUAGE_ID]?></span>
                    <?endforeach?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-content gallery-container no-before">
    <div class="page-desc" style="padding-bottom: 35px">
        <div class="container"><?=$arResult['PROPERTIES']['TEXT_'.LANGUAGE_ID]['~VALUE']['TEXT']?></div>
        <?if(!empty($arResult['PROPERTIES']['YOUTUBE']['VALUE'])):?>
            <div class="button-container">
                <span class="btn btn-black" data-change="photo"><?= Loc::getMessage("PHOTO") ?></span>
                <span class="btn float-right" data-change="video"><?= Loc::getMessage("VIDEO") ?></span>
            </div>
        <?endif?>
    </div>
    <div class="container pt-50">
        <div class="gallery-list row" data-show="photo">
            <?foreach($arResult['DISPLAY_PROPERTIES']['FILES']['FILE_VALUE'] as $file):?>
                <?$thumb = CFile::ResizeImageGet($file['ID'], array('width'=>300, 'height'=>180), BX_RESIZE_IMAGE_EXACT, true);?>
                <div class="item col-md-4 col-xs-12 col-sm-6">
                    <a class="box" href="<?=$file['SRC']?>" data-fancybox="gallery">
                        <div class="in">
                            <picture>
                                <source data-srcset="<?=$thumb['src']?>" type="image/webp"/>
                                <img class="lazy" data-src="<?=$thumb['src']?>" alt=""/>
                            </picture>
                        </div>
                    </a>
                </div>
            <?endforeach?>
        </div>
        <?if(!empty($arResult['PROPERTIES']['YOUTUBE']['VALUE'])):?>
            <div class="gallery-list row" data-show="video" style="display:none">
                <?foreach($arResult['PROPERTIES']['YOUTUBE']['~VALUE'] as $video):?>
                    <div class="item col-md-4 col-xs-12 col-sm-6">
                        <div class="video-resize">
                            <?=str_replace('src', 'class="lazy" data-src', $video)?>
                        </div>
                    </div>
                <?endforeach?>
            </div>
        <?endif?>
        <!--<div class="pagination mt-40">
            <ul>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><span>5</span></li>
                <li><a href="#">6</a></li>
                <li><a href="#">7</a></li>
                <li><a href="#">8</a></li>
            </ul>
        </div>-->
    </div>
</div>
