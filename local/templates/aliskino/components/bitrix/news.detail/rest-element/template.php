<?
$APPLICATION->IncludeComponent("sprint.editor:blocks", "content",
    array(
        'IBLOCK_ID' => $arResult['IBLOCK_ID'],
        'ELEMENT_ID' => $arResult['ID'],
        'PROPERTY_CODE' => 'BLOCKS_'.strtoupper(LANGUAGE_ID),
    ),
    false,
    array(
        'HIDE_ICONS' => 'Y'
    ))?>