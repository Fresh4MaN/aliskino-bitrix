<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>
<div class="pagination mt-40">
    <ul>

<?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>
    <li>
        <?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
            <span><?=$arResult["nStartPage"]?></span>
        <?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
            <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>"><?=$arResult["nStartPage"]?></a>
        <?else:?>
            <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a>
        <?endif?>
        <?$arResult["nStartPage"]++?>
    </li>
<?endwhile?>

    </ul>
</div>
