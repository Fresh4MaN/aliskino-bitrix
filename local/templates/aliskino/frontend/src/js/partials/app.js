let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);
var uikit = {
  lg: '1450',
  md: '1200',
  sm: '992',
  xs: '640',
  xxs: '480',
  ww: function () {
    return $(window).width();
  },
  wh: function () {
    return $(window).height();
  },
  select: function () {
    $('.form-select').each(function () {
      var pl = $(this).attr('placeholder'),
        search = ($(this).hasClass('is-search')) ? '' : Infinity,
        type = '',
        typeSelSize = '';
      if ($(this).hasClass('input-xs')) {
        typeSelSize = 'select2-dropdown_select_xs';
      }
      if ($(this).hasClass('border-white')) {
        type = 'border-white';
      }

      $(this).select2({
        language: 'ru',
        placeholder: pl,
        minimumResultsForSearch: search
      })
        .on('select2:open', function (e) {
          $('.select2-dropdown').addClass(typeSelSize);
          $('.select2-dropdown').addClass(type);
        });
    });


  },

  mask: function () {
    $("input[type='tel']").mask('+7 (000) 000-0000', {placeholder: '+7 (___) ___-____'});
  },

  validation: function () {
    var
      classValidate = 'is-validate',
      classParent = '.form-group',
      classError = 'is-error';

    function error(el) {
      $(el)
        .addClass(classError)
        .removeClass(classValidate)
        .closest(classParent)
        .addClass(classError)
        .removeClass(classValidate);
    }

    function validate(el) {
      $(el)
        .removeClass(classError)
        .addClass(classValidate)
        .closest(classParent)
        .removeClass(classError)
        .addClass(classValidate);
    }

    function reset(el) {
      $(el)
        .removeClass(classValidate + ' ' + classError)
        .closest(classParent)
        .removeClass(classError)
        .removeClass(classValidate + ' ' + classError)
    }

    $('.form-control').focus(function () {
      reset($(this))
    });
    $('select').change(function () {
      reset($(this))
    });
    $('input[type="checkbox"]').change(function () {
      reset($(this))
    });

    function checkInput(el) {
      var $form = $(el);
      $form.find('select.js-required').each(function () {
        if ($(this).val() != '') {
          validate($(this));
        } else {
          error($(this));
        }
      });
      $form.find('input[type=text].js-required').each(function () {
        if ($(this).val() != '') {
          validate($(this));
        } else {
          error($(this));
        }
      });
      $form.find('input[type=password].js-required').each(function () {
        if ($(this).val() != '') {
          validate($(this));
        } else {
          error($(this));
        }
      });
      if ($('.js-pass1', $form).length != 0) {
        var pass01 = $form.find('.js-pass1').val();
        var pass02 = $form.find('.js-pass2').val();
        if (pass01 == pass02) {
          validate($('.js-pass1, .js-pass2', $form));
        } else {
          error($('.js-pass1, .js-pass2', $form));
        }
      }
      $form.find('textarea.js-required').each(function () {
        if ($(this).val() != '') {
          validate($(this));
        } else {
          error($(this));
        }
      });
      $form.find('input[type=email]').each(function () {
        var regexp = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/i;
        var $this = $(this);
        if ($this.hasClass('js-required')) {
          if (regexp.test($this.val())) {
            validate($this);
          } else {
            error($this);
          }
        } else {
          if ($this.val() != '') {
            if (regexp.test($this.val())) {
              validate($this);
            } else {
              error($this);
            }
          } else {
            reset($this)
          }
        }
      });

      $form.find('input[type=checkbox].js-required').each(function () {
        if ($(this).is(':checked')) {
          validate($this);
        } else {
          error($this);
        }
      });
    }

    $('input[type="file"]').change(function () {
      $(this).closest('.input-file').addClass('choice').find('.choice').text($(this).val())
    });

    $('.js-submit').click(function () {
      var $form = $(this).closest('form');
      checkInput($form);
      var errors = $form.find('.is-error:visible').length;
      if (errors) {
        return false;
      }
    });
  },

  footerEmpty: function () {
    if ($('.footer-empty').length) {
      $('.footer-empty').height($('footer').outerHeight());
    }
  },

  tabs: function () {
    $('[data-tab]').click(function (e) {
      e.preventDefault();
      let tab = typeof ($(this).attr('href')) != 'undefined' ? $(this).attr('href') : $(this).attr('data-tab');
      if (typeof ($(this).attr('data-parent')) != 'undefined') {
        $('[href="' + tab + '"], [data-tab="' + tab + '"]').closest($(this).attr('data-parent')).addClass('is-active').siblings().removeClass('is-active');
      } else {
        $(this).addClass('is-active').siblings().removeClass('is-active');
      }
      $(tab).addClass('is-visible').siblings().removeClass('is-visible');
    });

  },

  lazy: function () {

    function logElementEvent(eventName, element) {
    }

    var callback_enter = function (element) {
    };
    var callback_exit = function (element) {
    };
    var callback_loading = function (element) {
    };
    var callback_loaded = function (element) {
    };
    var callback_error = function (element) {
    };
    var callback_finish = function () {
    };
    var callback_cancel = function (element) {

    };

    var lazyLoadOb = new LazyLoad({
      class_applied: "lz-applied",
      class_loading: "lz-loading",
      class_loaded: "lz-loaded",
      class_error: "lz-error",
      class_entered: "lz-entered",
      class_exited: "lz-exited",
      // Assign the callbacks defined above
      callback_enter: callback_enter,
      callback_exit: callback_exit,
      callback_cancel: callback_cancel,
      callback_loading: callback_loading,
      callback_loaded: callback_loaded,
      callback_error: callback_error,
      callback_finish: callback_finish
    });
    lazyLoadOb.update();
  },
  mainBanner: function () {
    if ($('body').hasClass('homepage')) {
      let speedChangeSlide = 300;
      let flag = true;
      let options = {
        swipe: false,
        speed: speedChangeSlide,
        lazyLoad: 'progressive',
        vertical: true,
        prevArrow: '.main-banner .slick-prev',
        nextArrow: '.main-banner .slick-next',
        cssEase: 'linear',
        verticalSwiping: true,
        dots: true,
        customPaging: function (slider, i) {
          let slidesCount = slider.slideCount < 10 ? 0 + '' + slider.slideCount : slider.slideCount
          let n = i + 1 < 10 ? '0' + (i + 1) : i + 1;
          return '<button> <svg class="icon icon_star"><use xlink:href="/local/templates/aliskino/frontend/build/images/sprite-svg.svg#star"></use></svg>' + n + ' / ' + slidesCount + '</button>';
        },
      };
      let slider = $(".main-banner .slider").slick(options)
        .on("lazyLoaded", function (e, slick, image, imageSource) {
          parentSlide = $(image).parent(".slick-slide");
          imageSource.src = image.attr("src"); //get source
          parentSlide.css("background-image", 'url("' + imageSource + '")').addClass("loaded"); //replace with background instead
          image.remove(); // remove source
        })
        .on('afterChange', function (event, slick, currentSlide, nextSlide) {
          setTimeout(function () {
            flag = true;
          }, 500)
        });

      let clearTimeOutScr;
      $('.main-banner').swipe({
        swipe: function (event, phase, direction, distance, duration, fingerCount, fingerData) {

          if (direction > 20) {
            if (phase == 'down') {
              //сработает при движении вверх
              if (flag) {
                flag = false;
                slider.slick('slickPrev');
              }
            }
            if (phase == 'up') {
              if (flag) {
                flag = false;
                slider.slick('slickNext');
              }
            }
          }
        },
        triggerOnTouchEnd: true,
        threshold: 20
      });
      $(window).bind('mousewheel DOMMouseScroll MozMousePixelScroll', function (event) {
        delta = parseInt(event.originalEvent.wheelDelta || -event.originalEvent.detail);
        if (flag) {
          flag = false;
          if (delta >= 0) {
            slider.slick('slickPrev');
          } else {
            slider.slick('slickNext');
          }
        }

      });
    }
  },

  mobileMenu: function () {
    $('.burger').click(function (e) {
      e.preventDefault();
      $('html').toggleClass('menu-open');
    });
  },

  fancybox: function () {
    $(".fancybox").fancybox({
      // Options will go here
      iframe: {
        preload: false
      }
    });
  },

  anTabs: function () {
    $('.announcement-tabs .tabs-panel .col a').click(function (e) {
      e.preventDefault();
      $(this).closest('.col').addClass('active').siblings().removeClass('active');
      $($(this).attr('href')).addClass('is-visible').siblings().removeClass('is-visible');
    });
    $('.announcement-tabs .slider-wrap .slider').slick({
      lazyLoad: 'progressive',
      dots: true,
      autoplay: true,
      autoplaySpeed: 4000
    });
  },

  bags: function () {

  },

  hourseGallery: function () {

    $('.hourse-gallery .carousel').slick({
      dots: true,
      arrows: false,
      infinite: false,
      slidesToShow: 2,
      slidesToScroll: 1,
      lazyLoad: 'progressive',
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,

          }
        },

        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });

  },

  vFilter: function () {
    $('.vacancies-container .filter-block a').click(function (e) {
      e.preventDefault();
      $(this).addClass('active').siblings().removeClass('active');
      $('.vacancies-list .item').hide();
      $('.vacancies-list').find($(this).attr('data-filter')).show();
    });
  },

  news: function () {
    $('.news-list-wrap .list .item .box .more a').click(function (e) {
      e.preventDefault();
      var txt = !$(this).hasClass('is-active') ? 'Свернуть >>' : 'Читать Подробнее >>'
      $(this).text(txt).toggleClass('is-active').closest('.box').toggleClass('is-visible');


    });
  },

  eventsCarousel: function () {
    $('.events-carousel').slick({
      lazyLoad: 'progressive',
      infinite: false,
      arrows: false,
      dots: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        }
      ]
    });
  },

  anchore: function () {
    $('.anchore-half-height').bind('click', function (event) {
      var $anchor = $(this).attr('href');
      if ($anchor.length > 0) {
        $('html, body').stop().animate({
          scrollTop: $(window).scrollTop() + $(window).height() / 2
        }, 1200);
        event.preventDefault();
      }
    });
  },

  selectChange: function () {
    $('.form-select').on('focus', function () {
      $(this).closest('.form-group').addClass('select-change')
    })
    $('.form-select').on('change blur', function () {
      if ($(this).find('option:selected').text() != '') {
        $(this).closest('.form-group').addClass('select-change')
      } else {
        $(this).closest('.form-group').removeClass('select-change')
      }
    })
  },

  pageSearch: function () {
    $('header .search-btn').click(function (e) {
      e.preventDefault();
      $('html').addClass('is-search-visible')
    });
    $('.search-container .overflay').click(function (e) {
      e.preventDefault();
      $('html').removeClass('is-search-visible')
    });
  },

  headerScr: function () {
    let scrTop = $(window).scrollTop();
    if (scrTop > 32) {
      $('header').addClass('is-fixed')
    } else {
      $('header').removeClass('is-fixed')
    }
  },

  mainInit: function () {
    this.lazy();
    this.select();
    this.validation();
    this.footerEmpty();
    this.tabs();
    this.fancybox();
    this.mask();
    this.mainBanner();
    this.mobileMenu();
    this.bags();
    this.anTabs();
    this.vFilter();
    this.hourseGallery();
    this.news();
    this.eventsCarousel();
    this.anchore();
    this.selectChange();
    this.pageSearch();
    this.headerScr();
  }
};
$(document).ready(function () {
  uikit.mainInit();
});
var clrTimeOut;
$(window).on('load', function (e) {
  clearTimeout(clrTimeOut);
  clrTimeOut = setTimeout(function () {
    uikit.footerEmpty();
  }, 200);
});

$(window).resize(function () {
  clearTimeout(clrTimeOut);
  clrTimeOut = setTimeout(function () {
    uikit.footerEmpty();
  }, 200);

});

$(window).scroll(function () {
  uikit.headerScr();
});
//
// // height for background image fixed
// var bg = $(".main-banner .item");
//
// function resizeBackground() {
//   bg.height($(window).height());
// }

// $(window).resize(resizeBackground);
// resizeBackground();
