<?
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Localization\Loc;
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
global $APPLICATION;
if($APPLICATION->GetCurPage(false) !== '/'):?>
    <footer>
        <div class="container">
            <div class="footer-in">
                <div class="col">
                    <a class="logo" href="/">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => "/local/includes/footer_svg.php"
                            )
                        );?>
                    </a>
                </div>
                <div class="col">
                    <div class="ttl upper bold mb-25 mt-25 mt-60_xs"><?= Loc::getMessage("NAME") ?></div>
                    <div class="copyright fs-11 upper"><?= Loc::getMessage("RIGHTS") . date('Y');?></div>
                </div>
                <div class="col">
                    <div class="ttl upper bold mb-25 mt-25 mt-60_xs"><?= Loc::getMessage("CONTACTS_TITLE") ?></div>
                    <div class="phone light"><a href="tel:<?=\Aliskino\CUtil::getPhoneForLinks(\Aliskino\Helper::getSetting('phone'))?>"><?=\Aliskino\Helper::getSetting('phone')?></a></div>
                    <div class="mail light"><a class="lnk_reverse" href="mailto:<?=\Aliskino\Helper::getSetting('email')?>"><?=\Aliskino\Helper::getSetting('email')?></a></div>
                </div>
                <div class="col">
                    <div class="ttl upper bold mb-25 mt-25 mt-60_xs"><?= Loc::getMessage("LOCATION_TITLE") ?></div>
                    <div class="address"><?=\Aliskino\Helper::getSetting('addr_'.LANGUAGE_ID)?><br><?=\Aliskino\Helper::getSetting('coords')?></div>
                </div>
            </div>
        </div>
    </footer>
<?endif?>
<div class="search-container">
    <div class="overflay"></div>
    <div class="container">
        <form action="/search/" method="get">
            <div class="form-group">
                <input type="text" name="q" placeholder="Поиск по сайту" value="<?=$_REQUEST['q']?>">
            </div>
            <button class="btn" type="submit"><?= Loc::getMessage("FIND") ?><svg class="icon icon_search">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#search"></use>
                </svg>
            </button>
        </form>
    </div>
</div>
</body>
</html>