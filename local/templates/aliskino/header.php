<?
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Localization\Loc;
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
global $APPLICATION;
?>
<!DOCTYPE html>
    <html>
        <head>
            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-52Z3KLD');</script>
            <!-- End Google Tag Manager -->
            <meta charset="utf-8">
            <title><?$APPLICATION->ShowTitle(false);?></title>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="format-detection" content="telephone=no">
            <meta name="keywords" content="<?=$APPLICATION->GetDirProperty("keywords");?>">
            <!-- appstore links-->
            <!-- seo meta-->
            <meta name="keywords" content="<?=$APPLICATION->GetDirProperty("keywords");?>">
            <meta name="author" content="Алискино">
            <!-- og meta-->
            <meta property="og:title" content="<?$APPLICATION->ShowTitle(false);?>">
            <meta property="og:url" content="<?=$APPLICATION->GetCurPage(false)?>">
            <meta property="og:image" content="">
            <meta property="og:description" content="<?$APPLICATION->ShowProperty("description")?>">
            <meta name="twitter:title" content="<?$APPLICATION->ShowTitle(false);?>">
            <meta name="twitter:description" content="<?$APPLICATION->ShowProperty("description")?>">
            <meta name="twitter:image:src" content="">
            <meta name="twitter:url" content="<?=$APPLICATION->GetCurPage(false)?>">
            <link rel="icon" type="image/png" href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/favicon-16x16.png" sizes="16x16">
            <!-- remove for production-->
            <meta name="robots" content="noindex">
            <link rel="preload stylesheet"  href="<?=SITE_TEMPLATE_PATH.\Aliskino\Constants::getFrontendDirectory(). 'css/fonts.css'?>"  as="style" >
            <?
            Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory() . 'css/template_styles.css');
            Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory() . 'js/script.js');
            Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory() . 'js/script.js');
            Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/custom_js/custom.js');
            Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/custom_css/custom.css');
            $APPLICATION->ShowHead();
            ?>
        </head>
        <body class="<?=$APPLICATION->GetDirProperty("body_class");?>">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-52Z3KLD"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

            ym(74477593, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
            });
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/74477593" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
        <div id="panel">
            <?$APPLICATION->ShowPanel();?>
        </div>
        <header>
            <div class="container">
                <button class="burger"><span></span></button>
                <button class="search-btn">
                    <svg class="icon icon_search">
                        <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#search"></use>
                    </svg>
                </button>
                <a class="logo" href="/">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/local/includes/header_svg.php"
                        )
                    );?>
                </a>
                <div class="mobile-dropdown">
                    <div class="scr-block">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "top",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "top1_".LANGUAGE_ID,
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => array(""),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "top1_".LANGUAGE_ID,
                                "USE_EXT" => "Y"
                            )
                        );?>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "top",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "top2_".LANGUAGE_ID,
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => array(""),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "top2_".LANGUAGE_ID,
                                "USE_EXT" => "Y"
                            )
                        );?>
                        <div class="bottom">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/local/includes/socialbox.php"
                                )
                            );?>
                            <ul class="languages">
                                <li><?if(LANGUAGE_ID == 'en') echo '<a href="?lang_ui=ru">';?>рус<?if(LANGUAGE_ID == 'en') echo '</a>';?></li>
                                <li><?if(LANGUAGE_ID == 'ru') echo '<a href="?lang_ui=en">';?>ENG<?if(LANGUAGE_ID == 'ru') echo '</a>';?></li>
                            </ul>
                        </div>
                        <div class="mobile-info d-none d-block_xs d-block_sm text-center pb-40">
                            <div class="ttl mt-40 mb-20 upper extrabold"><?= Loc::getMessage("CONTACTS_TITLE") ?></div>
                            <div class="phone light"><a href="tel:<?=\Aliskino\CUtil::getPhoneForLinks(\Aliskino\Helper::getSetting('phone'))?>"><?=\Aliskino\Helper::getSetting('phone')?></a></div>
                            <div class="mail light"><a href="mailto:<?=\Aliskino\Helper::getSetting('email')?>"><?=\Aliskino\Helper::getSetting('email')?></a></div>
                            <div class="ttl mt-40 mb-20 upper extrabold"><?= Loc::getMessage("LOCATION_TITLE") ?></div>
                            <div class="address"><?=\Aliskino\Helper::getSetting('addr_'.LANGUAGE_ID)?><br><?=\Aliskino\Helper::getSetting('coords')?></div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
