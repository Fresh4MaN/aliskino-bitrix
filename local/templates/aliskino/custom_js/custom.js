$(function(){
    $('#show-more-news').click(function(){
        var page = Number.parseInt($(this).data('page'));
        var count = Number.parseInt($(this).data('count'));
        $.ajax({
            dataType: 'text',
            data: {
                PAGEN_1: page+1,
                IS_AJAX: 'Y'
            },
            success: function(msg){
                $('.news-list-wrap .list').append(msg);
                uikit.lazy();
                if(page+1 < count){
                    $('#show-more-news').data('page',page+1);
                }else{
                    $('#show-more-news').hide();
                }
            },
            error: function(jqxhr, status, errorMsg) {
                console.log("Статус: " + status + " Ошибка: " + errorMsg)
            },

        });
        return false;
    });

    $('#show-more-team').click(function(){
        var page = Number.parseInt($(this).data('page'));
        var count = Number.parseInt($(this).data('count'));
        $.ajax({
            dataType: 'text',
            data: {
                PAGEN_1: page+1,
                IS_AJAX: 'Y'
            },
            success: function(msg){
                $('.worker:last').after(msg);
                uikit.lazy();
                if(page+1 < count){
                    $('#show-more-team').data('page',page+1);
                }else{
                    $('#show-more-team').hide();
                }
            },
            error: function(jqxhr, status, errorMsg) {
                console.log("Статус: " + status + " Ошибка: " + errorMsg)
            },

        });
        return false;
    });

    $('body').on('click', '.event-form [type="submit"]', function(){
        if($('#checkbox-3:checked').length == 0) {
            $('#label-for-checkbox').addClass('red-error');
            return false;
        }

        var $that = $('.event-form');
        var formData = new FormData($that.get(0));
        $.ajax({
            dataType: 'json',
			url: '/local/ajax/controllers/processEventFeedback.php',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function(msg){
                $('.fancybox-close-small').click();
                $('[href="#popup-report"]').click();
            }
        });
        return false;
    });

    $('#feedback-form').submit(function(){
        if($('#checkbox-3:checked').length == 0){
            $('#label-for-checkbox').addClass('red-error');
            return false;
        }

        var $that = $(this),
            formData = new FormData($that.get(0));
        $.ajax({
            dataType: 'json',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function(msg){
                $('[href="#popup-report"]').click();
            }
        });
        return false;
    });

    $('.subscription form').submit(function(){
        if($('#checkbox-3:checked').length == 0) {
            $('#label-for-checkbox').addClass('red-error');
            return false;
        }

        var that = $(this);

        $.ajax({
            dataType: 'json',
            type: "POST",
            data: that.serializeArray(),
            success: function(msg){
                $('[href="#popup-report"]').click();
            }
        });
        return false;
    });

    $('#checkbox-3').change(function(){
        if($('#checkbox-3:checked').length == 0){
            $('[type="submit"]').attr('disabled', 'true');
            $('#label-for-checkbox').addClass('red-error');
        }else{
            $('[type="submit"]').removeAttr('disabled');
            $('#label-for-checkbox').removeClass('red-error');
        }
    });

    $('.anchore-full-height').bind('click', function(event) {
        var $anchor = $(this).attr('href');
        if ($anchor.length>0) {
            $('html, body').stop().animate({
                scrollTop: $(window).scrollTop() + $(window).height()
            }, 1200);
            event.preventDefault();
        }
    });

    $('.button-container .btn').click(function(){
        var attr = $(this).data('change');
        $(this).addClass('btn-black').siblings().removeClass('btn-black');
        $('.gallery-list.row').hide();
        $('.gallery-list.row[data-show="'+attr+'"]').show();
    });
});

function setheadersPopup(msg){
    if(msg.status == 'error'){

    }
}
