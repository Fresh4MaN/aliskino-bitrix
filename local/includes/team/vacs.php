<div class="pict d-none_xs">
    <picture>
        <source data-srcset="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/pict-w.webp" type="image/webp"/>
        <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/pict-w.jpg" alt=""/>
    </picture>
</div>
