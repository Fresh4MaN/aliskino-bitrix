<div class="page-content evetns-container">
    <div class="container">
        <div class="page-head">
            <div class="page-title text-center_xs">
                <h1>Мероприятия <br> КСК Алискино</h1>
            </div>
        </div>
    </div>
    <div class="desc-pict bg-light">
        <div class="container">
            <div class="pict">
                <picture>
                    <?$img = \Aliskino\Helper::getSetting('UF_EVENT_MAIN', 'askaron.settings')?>
                    <source data-srcset="<?=CFile::GetPath($img)?>" type="image/webp"/>
                    <img class="lazy" data-src="<?=CFile::GetPath($img)?>" alt=""/>
                </picture>
            </div>
            <div class="desc">
                <p>Каждый из нас понимает очевидную вещь: новая модель организационной деятельности предопределяет высокую востребованность распределения внутренних резервов и ресурсов. В частности, убеждённость некоторых оппонентов предопределяет высокую востребованность дальнейших направлений развития. Вот вам яркий пример современных тенденций - высококачественный прототип будущего проекта говорит о возможностях экспериментов, поражающих по своей масштабности и грандиозности.</p>
            </div>
        </div>
    </div>
    <div class="picts-block d-flex flex-flow-row-wrap">
        <?$arImgs = \Aliskino\Constants::getEventImgs();?>
        <?foreach($arImgs as $img):?>
            <?$file = CFile::ResizeImageGet($img, array('width'=>320, 'height'=>270), BX_RESIZE_IMAGE_EXACT, true);?>
            <a href="<?=CFile::GetPath($img);?>" data-fancybox="about-desc">
                <picture>
                    <source data-srcset="<?=$file['src']?>" type="image/webp"/>
                    <img class="lazy" data-src="<?=$file['src']?>" alt=""/>
                </picture>
            </a>
        <?endforeach?>
    </div>
    <div class="desc-bottom bg-light">
        <div class="container">
            <p>Каждый из нас понимает очевидную вещь: новая модель организационной деятельности предопределяет высокую востребованность распределения внутренних резервов и ресурсов. В частности, убеждённость некоторых оппонентов предопределяет высокую востребованность дальнейших направлений развития. Вот вам яркий пример современных тенденций - высококачественный прототип будущего проекта говорит о возможностях экспериментов, поражающих по своей масштабности и грандиозности.</p>
        </div>
    </div>
</div>
