<div class="page-content evetns-container">
    <div class="container">
        <div class="page-head">
            <div class="page-title text-center_xs">
                <h1>Aliskino <br> events</h1>
            </div>
        </div>
    </div>
    <div class="desc-pict bg-light">
        <div class="container">
            <div class="pict">
                <picture>
                    <?$img = \Aliskino\Helper::getSetting('UF_EVENT_IMGS', 'askaron.settings')?>
                    <source data-srcset="<?=CFile::GetPath($img)?>" type="image/webp"/>
                    <img class="lazy" data-src="<?=CFile::GetPath($img)?>" alt=""/>
                </picture>
            </div>
            <div class="desc">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
            </div>
        </div>
    </div>
    <div class="picts-block d-flex flex-flow-row-wrap">
        <?$arImgs = \Aliskino\Constants::getEventImgs();?>
        <?foreach($arImgs as $img):?>
            <?$file = CFile::ResizeImageGet($img, array('width'=>320, 'height'=>270), BX_RESIZE_IMAGE_EXACT, true);?>
            <a href="<?=CFile::GetPath($img);?>" data-fancybox="about-desc">
                <picture>
                    <source data-srcset="<?=$file['src']?>" type="image/webp"/>
                    <img class="lazy" data-src="<?=$file['src']?>" alt=""/>
                </picture>
            </a>
        <?endforeach?>
    </div>
    <div class="desc-bottom bg-light">
        <div class="container">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
        </div>
    </div>
</div>
