<ul class="socialbox">
    <?if(!empty(\Aliskino\Helper::getSetting('yt'))):?>
        <li>
            <a target="_blank" href="<?=\Aliskino\Helper::getSetting('yt')?>">
                <svg class="icon icon_yt">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#yt"></use>
                </svg>
            </a>
        </li>
    <?endif?>
    <?if(!empty(\Aliskino\Helper::getSetting('inst'))):?>
        <li>
            <a target="_blank" href="<?=\Aliskino\Helper::getSetting('inst')?>">
                <svg class="icon icon_inst">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#inst"></use>
                </svg>
            </a>
        </li>
    <?endif?>
    <?if(!empty(\Aliskino\Helper::getSetting('tg'))):?>
        <li>
            <a target="_blank" href="<?=\Aliskino\Helper::getSetting('tg')?>">
                <svg class="icon icon_tg">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#tg"></use>
                </svg>
            </a>
        </li>
    <?endif?>
    <?if(!empty(\Aliskino\Helper::getSetting('fb'))):?>
        <li>
            <a target="_blank" href="<?=\Aliskino\Helper::getSetting('fb')?>">
                <svg class="icon icon_fb">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#fb"></use>
                </svg>
            </a>
        </li>
    <?endif?>
</ul>