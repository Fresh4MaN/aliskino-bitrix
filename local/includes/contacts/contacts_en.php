<div class="col-xs-12 col-md-6">
    <div class="info row">
        <div class="itm col-sm-6 col-xs-12">
            <div class="ttl upper extrabold">
                <svg class="icon icon_star">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#star"></use>
                </svg>
                Location
            </div>
            <div class="txt mt-20"><?=\Aliskino\Helper::getSetting('addr_'.LANGUAGE_ID)?></div>
        </div>
        <div class="itm col-sm-6 col-xs-12 mt-20_xs">
            <div class="ttl upper extrabold">
                <svg class="icon icon_star">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#star"></use>
                </svg>
                Contacts
            </div>
            <div class="txt mt-20">
                <div class="phone"><a href="tel:<?=\Aliskino\CUtil::getPhoneForLinks(\Aliskino\Helper::getSetting('phone'))?>"><?=\Aliskino\Helper::getSetting('phone')?></a></div>
                <div class="mail"><a href="mailto:<?=\Aliskino\Helper::getSetting('email')?>"><?=\Aliskino\Helper::getSetting('email')?></a></div>
            </div>
        </div>
        <div class="itm col-sm-6 mt-50 col-xs-12 mt-20_xs">
            <div class="ttl upper extrabold">
                <svg class="icon icon_star">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#star"></use>
                </svg>
                GPS coords
            </div>
            <div class="txt mt-20"><?=\Aliskino\Helper::getSetting('coords')?></div>
            <div class="reck upper mt-20"><a href="<?=CFile::GetPath(\Aliskino\Helper::getSetting('UF_REKVIZ', 'askaron.settings'))?>" download>Реквизиты >></a></div>
        </div>
        <div class="itm col-sm-6 mt-50 col-xs-12 d-none_xs">
            <picture>
                <source data-srcset="<?=CFile::GetPath(\Aliskino\Helper::getSetting('UF_CONT_QR', 'askaron.settings'))?>" type="image/webp"/>
                <img class="lazy" data-src="<?=CFile::GetPath(\Aliskino\Helper::getSetting('UF_CONT_QR', 'askaron.settings'))?>" alt=""/>
            </picture>
        </div>
    </div>
</div>
