<?
global $APPLICATION;
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle("Конноспортивный комплекс «Алискино» в Москве.");
?>
    <div class="main-banner">
        <button class="slick-prev slick-arrow">
            <svg class="icon icon_arrow-right">
                <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#arrow-right"></use>
            </svg>
        </button>
        <button class="slick-next slick-arrow">
            <svg class="icon icon_arrow-right">
                <use xlink:href="<?=SITE_TEMPLATE_PATH . \Aliskino\Constants::getFrontendDirectory()?>images/sprite-svg.svg#arrow-right"></use>
            </svg>
        </button>
        <div class="slider">
            <?$APPLICATION->IncludeComponent(
                "aliskino:index.page",
                "",
                Array()
            );?>
        </div>
    </div>
<?$APPLICATION->IncludeComponent(
    "bitrix:subscribe.edit",
    "hidden",
    Array(
        "SHOW_HIDDEN" => "N",
        "ALLOW_ANONYMOUS" => "Y",
        "SHOW_AUTH_LINKS" => "Y",
        "CACHE_TIME" => "36000000",
        "SET_TITLE" => "N"
    )
);?>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>